package com.example.searchscreen

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import java.util.*

class JobCollectionAdapter(var listOfJobs : Hashtable<Int, JobsList>?,var onItemClick:OnItemClick) : RecyclerView.Adapter<JobCollectionAdapter.ViewHolder>() {

    interface OnItemClick{
        fun onItemClick(jobId : Long?)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view:View = LayoutInflater.from(parent.context).inflate(R.layout.job_item,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        if(listOfJobs == null){
            return 0
        }
        return listOfJobs!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var jobsList : JobsList? = listOfJobs?.get(position)
        holder.jobTitle.text = jobsList?.jobTitle
        holder.cityState.text = jobsList?.city + ","+jobsList?.state
        holder.distance.text = jobsList?.distance
        if(jobsList!!.jobType.equals("O"))
            holder.jobType.text = "One Time Job";
        else if(jobsList!!.jobType.equals("OC"))
            holder.jobType.text = "Occasional";
        else
            holder.jobType.text = "Recurring Job";
        holder.jobRate.text = "$"+jobsList?.hourlyRateFrom.toString()+ " - $" + jobsList?.hourlyRateTo.toString()
        holder.jobTitle.setBackgroundColor(holder.jobTitle.context.resources.getColor(R.color.transparent));
        holder.cityState.setBackgroundColor(holder.cityState.context.resources.getColor(R.color.transparent));
//        viewHolderItem.tvDate.setBackgroundDrawable(getResources().getDrawable(R.drawable.empty_text_bg_corners_rounded_grey_bg_no_border_small));
        if(jobsList!!.isNew){
            holder.isNew.visibility = View.VISIBLE
        }
        holder.itemView.setOnClickListener(object : View.OnClickListener{
            override fun onClick(p0: View?) {
                onItemClick.onItemClick(jobsList?.jobId)
            }

        })
    }

    class ViewHolder(itemView:View) : RecyclerView.ViewHolder(itemView) {

        var jobTitle : TextView = itemView.findViewById(R.id.job_title)
        var cityState : TextView = itemView.findViewById(R.id.city_state)
        var distance : TextView = itemView.findViewById(R.id.dist_text)
        var jobType : TextView = itemView.findViewById(R.id.job_type_text)
        var jobRate : TextView = itemView.findViewById(R.id.rate_text)
        var isNew : TextView = itemView.findViewById(R.id.tv_new)
    }

}