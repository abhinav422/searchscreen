package com.example.searchscreen

data class JobDetails(val jobTitle:String,val city : String,val jobZip:String,
                      val jobType:String,val jobCategory: String,val jobPostDate:String,
                      val jobStartDate : String,val postJobUntil:String,val description:String,
                      val jobId:Long,val jobSatus:String,val isReceivingApplications:Boolean,
                      val isDateNightJob:Boolean,val isTransportationJob:Boolean,
                      val transportationNumber:Long,val isFeatured:Boolean,val isPostedFromHKWidget:Boolean,
                      val isNew:Boolean,val applicationLimitMet:Boolean,val isFullTime:Boolean,
                      val hourlyRateFrom:String,val hourlyRateTo:String,val nnaJob:Boolean,val serviceId:String,
                      val serviceName:String,val distance:Long,val distanceText:String,val cannotApplyReason:String,
                      val coverLetterTemplate:String)