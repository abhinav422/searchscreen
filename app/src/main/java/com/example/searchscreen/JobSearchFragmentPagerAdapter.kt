package com.example.searchscreen

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class JobSearchFragmentPagerAdapter(var fm : FragmentManager) : FragmentPagerAdapter(fm){

    companion object {
        private  val TOTAL_COUNT : Int = 3
    }

    override fun getItem(position: Int): Fragment {
       lateinit var jobListFragment : JobListFragment
        if(position==0){
            jobListFragment = JobListFragment.getInstance("BEST")
        }
        else if(position==1){
            jobListFragment = JobListFragment.getInstance("NEW")
        }
        else if(position==2){
            jobListFragment = JobListFragment.getInstance("NEARBY")
        }
        return jobListFragment
    }

    override fun getPageTitle(position: Int): CharSequence? {
        if(position==0){
            return "BEST"
        }
        else if(position==1){
            return "NEW"
        }
        else {
            return "NEARBY"
        }
    }

    override fun getCount(): Int {
        return TOTAL_COUNT
    }


}


