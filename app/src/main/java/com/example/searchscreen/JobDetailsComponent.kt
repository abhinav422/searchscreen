package com.example.searchscreen

import dagger.Component

@JobDetailsActivityScope
@Component(dependencies = arrayOf(AppComponent::class),modules = arrayOf(JobDetailsComponentModule::class))
interface JobDetailsComponent{

    fun inject(jobDetailsViewModel: JobDetailsViewModel)
}