package com.example.searchscreen

import android.arch.core.util.Function
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import com.google.gson.Gson
import org.json.JSONObject
import javax.inject.Inject

class JobDetailsSearch @Inject constructor(var apiRequest: ApiRequest){


    fun startRequest(jobId: Long) {
        apiRequest.additionalPath = "/"+jobId.toString()
        apiRequest.startRequest(null)
    }

    lateinit var jobsDetailLiveData: LiveData<JobDetails>
    init{
       jobsDetailLiveData = Transformations.switchMap(apiRequest.mutableLiveData,object: Function<JSONObject, LiveData<JobDetails>>{
           override fun apply(input: JSONObject?): MutableLiveData<JobDetails> {
               var gson:Gson= Gson()
               var jobDetailsLiveData:MutableLiveData<JobDetails> = MutableLiveData<JobDetails>()
               jobDetailsLiveData.value = gson.fromJson(input?.getJSONObject("data")?.getJSONObject("general").toString(),JobDetails::class.java)
               return jobDetailsLiveData
           }
       })
    }

}