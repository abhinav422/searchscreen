package com.example.searchscreen

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.searchscreen.patternlib.CustomTextView
import com.example.searchscreen.patternlib.NavigationItem
import java.util.ArrayList

class SearchRefineActivity: BaseActivity() {

    private lateinit var searchParams: SearchParams
    private val RESULT_CODE:Int = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search_refine_activity)
        searchParams = intent.extras.getSerializable("searchParams") as SearchParams
        initView()
    }

    override fun onBackPressed() {
        applyFilter()
        super.onBackPressed()
    }

    private fun applyFilter() {
        var bundle=Bundle()
        bundle.putSerializable("searchParams",searchParams)
        var intent=Intent()
        intent.putExtras(bundle)
        setResult(RESULT_CODE,intent)
    }

    private fun initView() {
        var navigationItem: NavigationItem
        navigationItem = findViewById(R.id.searchRefineVertical) as NavigationItem
        navigationItem.setTag(navigationItem.id + 1, "verticals")//String array name
        navigationItem.setTag(navigationItem.id + 2, "Type of Care")//Label string in Dialog
        navigationItem.setTag(navigationItem.id + 3, navigationItem.getCenterView())//CenterView
        (navigationItem.getCenterView() as CustomTextView).setText(searchParams.mServiceId)

        navigationItem = findViewById(R.id.searchRefineZip) as NavigationItem
        navigationItem.setTag(navigationItem.id + 1, null )//String array name
        navigationItem.setTag(navigationItem.id + 2, "Zip Code")//Label string in Dialog
        navigationItem.setTag(navigationItem.id + 3, navigationItem.centerView)//CenterView
        (navigationItem.centerView as CustomTextView).setText(searchParams.mZipCode)

        navigationItem = findViewById(R.id.searchRefineKeyword) as NavigationItem
        navigationItem.setTag(navigationItem.id + 1, null)//String array name
        navigationItem.setTag(navigationItem.id + 2, "Keyword")//Label string in Dialog
        navigationItem.setTag(navigationItem.id + 3, navigationItem.centerView)//CenterView
        (navigationItem.centerView as CustomTextView).setText(searchParams.mSearchFilter.mKeyword)

        navigationItem = findViewById(R.id.searchRefineDistance) as NavigationItem
        navigationItem.setTag(navigationItem.id + 1, "dist")//String array name
        navigationItem.setTag(navigationItem.id + 2, "Distance")//Label string in Dialog
        navigationItem.setTag(navigationItem.id + 3, navigationItem.centerView)//CenterView
        (navigationItem.centerView as CustomTextView).setText(formatDistance(searchParams.mDist))

        navigationItem = findViewById(R.id.searchRefineJobType) as NavigationItem
        navigationItem.setTag(navigationItem.id + 1, "job_types")//String array name
        navigationItem.setTag(navigationItem.id + 2, "Job Type")//Label string in Dialog
        navigationItem.setTag(navigationItem.id + 3, navigationItem.centerView)//CenterView
        (navigationItem.centerView as CustomTextView).setText(getJobTypeValue(searchParams.mSearchFilter.mJobType))

        navigationItem = findViewById(R.id.searchRefineHourlyRate) as NavigationItem
        navigationItem.setTag(navigationItem.id + 1, "hourly")//String array name
        navigationItem.setTag(navigationItem.id + 2, "Hourly rate")//Label string in Dialog
        navigationItem.setTag(navigationItem.id + 3, navigationItem.centerView)//CenterView
        (navigationItem.centerView as CustomTextView).setText(formatHourlyRate(searchParams.mSearchFilter.mHourly))

        navigationItem = findViewById(R.id.searchRefineNumberOfChildren) as NavigationItem
        navigationItem.setTag(navigationItem.id + 1, "num_of_child")//String array name
        navigationItem.setTag(navigationItem.id + 2, "Number of Children")//Label string in Dialog
        navigationItem.setTag(navigationItem.id + 3, navigationItem.centerView)//CenterView
        (navigationItem.centerView as CustomTextView).setText(NumOfChild.getNumOfChildKey(searchParams.mSearchFilter.mNumOfChildren))

        navigationItem = findViewById(R.id.searchRefineAgeOfChildren) as NavigationItem
        navigationItem.setTag(navigationItem.id + 1, "ages_of_child")//String array name
        navigationItem.setTag(navigationItem.id + 2, "Ages of Children")//Label string in Dialog
        navigationItem.setTag(navigationItem.id + 3, navigationItem.centerView)//CenterView
        (navigationItem.centerView as CustomTextView).setText(AgeOfChild.getAgeOfChildKey(searchParams.mSearchFilter.mChildrenAges))

        navigationItem = findViewById(R.id.searchRefineJobCatergory) as NavigationItem
        navigationItem.setTag(navigationItem.id + 1, "job_categories")//String array name
        navigationItem.setTag(navigationItem.id + 2, "Job Category")//Label string in Dialog
        navigationItem.setTag(navigationItem.id + 3, navigationItem.centerView)//CenterView
        val dispValue = if (searchParams.mSearchFilter.mJobCategory.isEmpty()) "Any" else if (searchParams.mSearchFilter.mJobCategory.equals("Company")) "Business" else "Family"
        (navigationItem.centerView as CustomTextView).text = dispValue
    }

    fun navItemClick(view: View) {
        val strArrayName = view.getTag(view.id + 1)
        if (strArrayName != null) {

            showPickerDialog(view, strArrayName as String)
        } else {
            showEditDialog(view)
        }
        Log.d("Hi", "hi")
    }

    private fun showEditDialog(view: View) {
        var editDialog : EditDialogFragment= EditDialogFragment((view.getTag(view.id+3) as CustomTextView).text.toString(),view.getTag(view.id+2) as String
        ,(view.getTag(view.id+2) as String).equals("Zip Code"),object:EditDialogFragment.OnInputCompleted{
            override fun onInputCompleted(text: String) {
                (view.getTag(view.id+3) as CustomTextView).text=text
                if(view.id==R.id.searchRefineKeyword){
                    searchParams.mSearchFilter.mKeyword=text
                }
                else if(view.id == R.id.searchRefineZip){
                    searchParams.mZipCode=text
                }

            }

        })
        editDialog.show(supportFragmentManager,"")
    }

    fun showPickerDialog(view: View, strArrayName: String) {
        val values: MutableList<String>
        val strings: Array<String>
        val resourceId = this.getResourceId(strArrayName, com.example.searchscreen.R.array::class.java)
        strings = resources.getStringArray(resourceId)
        var customTextView:CustomTextView = view.getTag(view.id+3) as CustomTextView
        values = ArrayList(strings.size)
        for (i in 0..strings.size-1) {
            values.add(strings[i].trim())
        }
        DialogListFragment(values,object:DialogListFragment.OnListItemClick{
            override fun onListItemClick(string: String) {
                if(view.id==R.id.searchRefineVertical){
                    searchParams.mServiceId=TypeOfCare.getEnumForName(string)!!.value

                }
                else if(view.id == R.id.searchRefineDistance){
                    var str = stripMiles(string)
                    if(str.equals("")){
                        str="10"
                    }
                    searchParams.mDist=str
                }
                else if(view.id == R.id.searchRefineHourlyRate){
                    searchParams.mSearchFilter.mHourly=stripDollar(removeAny(""))
                }
                else if(view.id==R.id.searchRefineJobType){
                    searchParams.mSearchFilter.mJobType=getJobType(string)
                }
                else if(view.id == R.id.searchRefineNumberOfChildren){
                    searchParams.mSearchFilter.mNumOfChildren=NumOfChild.getNumOfChildValue(string)
                }
                else if(view.id == R.id.searchRefineAgeOfChildren){
                    searchParams.mSearchFilter.mChildrenAges=AgeOfChild.getAgeOfChildValue(string)
                }
                else if(view.id == R.id.searchRefineJobCatergory){
                    searchParams.mSearchFilter.mJobCategory= if (string == "Any") "" else if (string == "Family") "Individual" else "Company";
                }
                customTextView.setText(string)
            }

        },view.getTag(view.id+2) as String).show(supportFragmentManager,"")
    }


    fun getResourceId(resourceName: String, c: Class<*>): Int {
        try {
            val idField = c.getDeclaredField(resourceName)
            return idField.getInt(idField)
        } catch (e: Exception) {
            throw RuntimeException("No resource ID found for: $resourceName / $c", e)
        }

    }

    private fun formatDistance(str: String): String {
        var str = str
        if (str.length > 0) {
            if (str == "1") {
                str = "1 mile"
            } else {
                str = "$str miles"
            }
        }

        return str
    }

    private fun getJobTypeValue(jobType: String?): String {
        var value = "Any"

        // Sanity check
        if (jobType == null) {
            return value
        }

        if (jobType == "O") {
            value = "One Time"
        } else if (jobType == "R") {
            value = "Regularly Scheduled"
        }

        return value
    }

    private fun formatHourlyRate(str: String): String {
        var str = str
        if (str.length > 0) {
            val parts = str.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

            if (parts.size == 2) {
                str = "$" + parts[0] + "-$" + parts[1]
            }
        }

        return str
    }

    private fun stripMiles(src: String): String {
        var src = src
        src = src.replace(" miles", "")
        src = src.replace(" mile", "")

        return src
    }

    private fun removeAny(src: String): String {
        return src.replace("Any", "")
    }

    private fun stripDollar(src: String): String {
        return src.replace("$", "")
    }

    private fun getJobType(value: String?): String {
        var jobType = ""

        // Sanity check
        if (value == null) {
            return jobType
        }

        if (value == "One Time" || value == "Occasional") {
            jobType = "O"
        } else if (value == "Regularly Scheduled") {
            jobType = "R"
        }

        return jobType
    }
}

