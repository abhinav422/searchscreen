package com.example.searchscreen

import android.annotation.SuppressLint
import android.support.v4.app.ListFragment
import android.view.View
import android.widget.ListView

class CustomListFragment (): ListFragment(){

    private lateinit var onListItemClick: OnListItemClick

    @SuppressLint("ValidFragment")
    constructor(onListItemClick: OnListItemClick):this(){
        this.onListItemClick=onListItemClick
    }
    interface OnListItemClick{
        fun onListItemClick(position: Int)
    }
    override fun onListItemClick(l: ListView?, v: View?, position: Int, id: Long) {
        super.onListItemClick(l, v, position, id)
        onListItemClick.onListItemClick(position)
    }
}