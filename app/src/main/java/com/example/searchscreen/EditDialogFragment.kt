package com.example.searchscreen

import android.annotation.SuppressLint
import android.support.v4.app.DialogFragment
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText

class EditDialogFragment():DialogFragment(){

    private var editText: EditText? = null
    private var button: Button?=null
    private lateinit var onInputCompleted: OnInputCompleted
    private  var isInputNumber:Boolean =false
    private lateinit var textToBeShown:String
    private lateinit var title:String
    interface OnInputCompleted{
        fun onInputCompleted(text:String)
    }

    @SuppressLint("ValidFragment")
    constructor(textToBeShown:String,title:String, isInputNumber: Boolean,onInputCompleted: OnInputCompleted):this(){
        this.textToBeShown=textToBeShown
        this.onInputCompleted=onInputCompleted
        this.isInputNumber=isInputNumber
        this.title=title
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.edit_dialog_fragment,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        editText=view.findViewById(R.id.editText)
        button=view.findViewById(R.id.edit_dialog_button)
        editText?.setText(textToBeShown)
        if(isInputNumber){
            editText?.inputType = InputType.TYPE_CLASS_NUMBER
        }
        button?.setOnClickListener(object:View.OnClickListener{
            override fun onClick(p0: View?) {
                if(editText?.text.toString().trim().length!=0) {
                    onInputCompleted.onInputCompleted(editText?.text.toString().trim())
                }
                dismiss()
            }
        })
    }
}