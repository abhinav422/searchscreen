package com.example.searchscreen

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.example.searchscreen.patternlib.TabIndicator

class SearchScreenActivity : AppCompatActivity() {

    private lateinit var viewPager : ViewPager
    public lateinit var searchScreenViewModel: SearchScreenViewModel
    private lateinit var tabLayout: TabLayout
    private lateinit var tabIndicator: TabIndicator
    private val  REQUEST_CODE: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("Check", "Step 1")
        setContentView(R.layout.search_screen)
        viewPager = findViewById(R.id.viewpager)
        tabLayout=findViewById(R.id.tablayout)
        tabIndicator=findViewById(R.id.tabindicator)
        searchScreenViewModel =  ViewModelProviders.of(this).get(SearchScreenViewModel::class.java)
        viewPager.adapter = JobSearchFragmentPagerAdapter(supportFragmentManager)
        tabIndicator.setupWithViewPager(tabLayout, viewPager)
        searchScreenViewModel.startRequest()

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_refine_activity, menu)
        return true

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if(item?.itemId==R.id.search_action_filter){
            var intent: Intent = Intent(this,SearchRefineActivity::class.java)
            intent.putExtra("searchParams",searchScreenViewModel.repository.bestJobSearch.searchParams)
            startActivityForResult(intent,REQUEST_CODE)
            return true
        }
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==REQUEST_CODE && resultCode==1){
            var searchParams:SearchParams = data?.extras?.getSerializable("searchParams") as SearchParams
            JobSearch.mServiceId = searchParams.mServiceId
            JobSearch.mKeyword = searchParams.mSearchFilter.mKeyword
            JobSearch.mZipCode = searchParams.mZipCode
            JobSearch.mDist = searchParams.mDist
            JobSearch.mHourlyRate = searchParams.mSearchFilter.mHourly
            JobSearch.mJobType = searchParams.mSearchFilter.mJobType
            JobSearch.mNumOfChild = searchParams.mSearchFilter.mNumOfChildren
            JobSearch.mAgeOfChild = searchParams.mSearchFilter.mChildrenAges
            JobSearch.mJobCategory = searchParams.mSearchFilter.mJobCategory
            searchScreenViewModel.clearJobSearch()
            searchScreenViewModel.startRequest()

        }
    }
}