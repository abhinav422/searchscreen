package com.example.searchscreen

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import java.util.*
import javax.inject.Inject

class SearchScreenViewModel  : ViewModel(),SearchParamsProviderInterface {


    @Inject
    lateinit var repository: Repository
    lateinit var searchScreenComponent: SearchScreenComponent
    lateinit var  bestJobsList: Hashtable<Int, JobsList>

    override fun getListOfParams(): ArrayList<Params>? {
       return null
    }

    private lateinit var newJobsList: Hashtable<Int, JobsList>

    init {
        searchScreenComponent = DaggerSearchScreenComponent.builder().appComponent(SearchScreenApplication.getInstance().appComponent).searchScreenComponent(SearchScreenComponentModule(this)).build()
        searchScreenComponent.inject(this)
        repository.bestMutableLiveData.observeForever(object :Observer<Hashtable<Int, JobsList>> {
            override fun onChanged(t: Hashtable<Int, JobsList>?) {
                bestJobsList = t!!
            }

        })
        repository.newMutableLiveData.observeForever(object :Observer<Hashtable<Int, JobsList>> {
            override fun onChanged(t: Hashtable<Int, JobsList>?) {
                newJobsList = t!!
            }

        })
    }

    fun startRequest(){
       repository.startRequest()
    }

    fun getLiveData(s: String): MutableLiveData<Hashtable<Int, JobsList>>? {
        if(s.equals("BEST"))
        {
            return repository.bestMutableLiveData
        }
        else if(s.equals("NEW"))
        {
            return repository.newMutableLiveData
        }
        else{
            return repository.nearByMutableLiveData
        }
    }

    fun clearJobSearch() {
        repository.clearJobSearch()
    }

    fun startRequest(sortBy: String,startFrom:Int) {
        repository.startRequest(sortBy,startFrom)
    }
}