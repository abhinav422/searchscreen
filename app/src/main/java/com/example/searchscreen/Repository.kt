package com.example.searchscreen

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import java.util.*
import javax.inject.Inject

class Repository  @Inject constructor(){


    @Inject
    lateinit var bestJobSearch: JobSearch

    @Inject
    lateinit var newJobSearch:JobSearch

    @Inject
    lateinit var nearbyJobSearch:JobSearch

    var bestMutableLiveData:MutableLiveData<Hashtable<Int,JobsList>> = MutableLiveData()

    var newMutableLiveData:MutableLiveData<Hashtable<Int,JobsList>> = MutableLiveData()

    var nearByMutableLiveData:MutableLiveData<Hashtable<Int,JobsList>> = MutableLiveData()



    init {

    }
    fun startRequest() {

        bestJobSearch.sortBy="BEST"
        bestJobSearch.startRequest()
        bestJobSearch.mutableLiveData.observeForever(object :Observer<Hashtable<Int, JobsList>> {
            override fun onChanged(t: Hashtable<Int, JobsList>?) {
                bestMutableLiveData.postValue(t)
            }

        })

        newJobSearch.sortBy="NEW"
        newJobSearch.startRequest()
        newJobSearch.mutableLiveData.observeForever(object :Observer<Hashtable<Int, JobsList>> {
            override fun onChanged(t: Hashtable<Int, JobsList>?) {
                newMutableLiveData.postValue(t)
            }

        })
        nearbyJobSearch.sortBy="NEARBY"
        nearbyJobSearch.startRequest()
        nearbyJobSearch.mutableLiveData.observeForever(object :Observer<Hashtable<Int, JobsList>> {
            override fun onChanged(t: Hashtable<Int, JobsList>?) {
                nearByMutableLiveData.postValue(t)
            }

        })

    }

    fun clearJobSearch() {
        bestJobSearch.clearJobSearch()
        bestJobSearch.mStart=0
        newJobSearch.clearJobSearch()
        newJobSearch.mStart=0
        nearbyJobSearch.clearJobSearch()
        nearbyJobSearch.mStart=0
    }

    fun startRequest(sortBy: String,startFrom:Int) {
        if(sortBy.equals("BEST")) {
            bestJobSearch.mStart = startFrom
            bestJobSearch.startRequest()
        }
        else if(sortBy.equals("NEW")) {
            newJobSearch.mStart = startFrom
            newJobSearch.startRequest()
        }
        else{
            nearbyJobSearch.mStart = startFrom
            nearbyJobSearch.startRequest()
        }
    }

}