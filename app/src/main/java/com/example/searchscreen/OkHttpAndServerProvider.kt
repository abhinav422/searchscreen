package com.example.searchscreen

import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response

class OkHttpAndServerProvider(){
    lateinit var okHttpClient: OkHttpClient
    lateinit var serverUrl:String
    init {
        okHttpClient = OkHttpClient.Builder().addInterceptor(object : Interceptor {
            override fun intercept(chain: Interceptor.Chain?): Response? {
                val request = chain?.request()
                val authenticatedRequest = request?.newBuilder()
                        ?.header("Authorization", Credentials.basic("remote","remoteUserP!"))?.build()

                return chain?.proceed(authenticatedRequest)

            }

        }).build()

        serverUrl = "https://www.uat1.carezen.net/platform/spi/"
    }

}