package com.example.searchscreen

data class JobsList(var imageURL : String,var jobTitle:String,var displayName : String,var city : String,var state : String,var distance :String,
                    var jobType : String, var jobCategory : String , var jobPostDate : String,var jobStartDate: String,var jobEndDate: String,
                    var postJobUntil : String,var hourlyRateFrom : String,var hourlyRateTo:String,var isNew: Boolean,var jobStatus:String,var isOnline : Boolean, var jobId:Long
                    ,var isFullTime : Boolean,var compensationType : String,var serviceId : String,var flatRate : Int,var recruitmentJob :Boolean,
                    var featured : Boolean,var companyName : String)