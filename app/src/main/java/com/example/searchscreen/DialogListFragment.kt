package com.example.searchscreen

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.ListFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.FrameLayout

class DialogListFragment() : DialogFragment(){

    interface OnListItemClick {
        fun onListItemClick(string:String)
    }
    lateinit var onListItemClick: OnListItemClick
    lateinit var list:ArrayList<String>
    lateinit var title:String
    @SuppressLint("ValidFragment")
    constructor(list:ArrayList<String>,onListItemClick: OnListItemClick,title:String):this(){
        this.list=list
        this.onListItemClick=onListItemClick
        this.title=title
    }
    private lateinit var frameLayout:FrameLayout
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_list_fragment,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog.setTitle(title)
        frameLayout=view.findViewById(R.id.list_fragment)
        var arrayAdapter: ArrayAdapter<String> = ArrayAdapter(view.context, android.R.layout.simple_list_item_1,list)
        var listFragment:ListFragment = CustomListFragment(object:CustomListFragment.OnListItemClick{
            override fun onListItemClick(position: Int) {
                onListItemClick.onListItemClick(list.get(position))
                dismiss()
            }

        })
        listFragment.listAdapter=arrayAdapter
        childFragmentManager.beginTransaction().add(R.id.list_fragment,listFragment).addToBackStack(null).commit()
    }
}