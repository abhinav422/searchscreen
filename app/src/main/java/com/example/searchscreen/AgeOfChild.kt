package com.example.searchscreen

enum class AgeOfChild(key:String,value:String){
    NEWBORNS("Newborns","CHLDAGEGP001"),TODDLERS("Toddlers","CHILDAGEGP002"),
    EARLY_SCHOOL_AGE("Early School Age","CHILDAGEGP003"),ELEMENTARY_SCHOOL_AGE("Elementary School Age", "CHLDAGEGP004"),
    TEENAGERS("Pre-Teens/Teenagers", "CHLDAGEGP005"),ANY("Any","");
    public val value:String=value
    public val key:String=key
    companion object{
        fun getAgeOfChildValue(text:String):String{
            val ageOfChild= AgeOfChild.values()
            for(i in 0..ageOfChild.size-1){
                if(text.equals(ageOfChild[i].key)){
                    return ageOfChild[i].value
                }
            }
            return ANY.value
        }

        fun getAgeOfChildKey(text:String):String{
            val ageOfChild= AgeOfChild.values()
            for(i in 0..ageOfChild.size-1){
                if(text.equals(ageOfChild[i].value)){
                    return ageOfChild[i].key
                }
            }
            return ANY.key
        }
    }

}