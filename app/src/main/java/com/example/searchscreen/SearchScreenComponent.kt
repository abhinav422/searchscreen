package com.example.searchscreen

import dagger.Component

@SearchScreenActvityScope
@Component(dependencies = arrayOf(AppComponent::class),modules = arrayOf(SearchScreenComponentModule::class))
interface SearchScreenComponent{

    fun inject(searchScreenViewModel: SearchScreenViewModel)

    @Component.Builder interface Builder{
        fun build(): SearchScreenComponent
        fun appComponent(appComponent: AppComponent):Builder
        fun searchScreenComponent(searchScreenComponentModule: SearchScreenComponentModule):Builder
    }
}