package com.example.searchscreen

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.example.searchscreen.patternlib.CareTextBlock

class JobDetailsActivity:BaseActivity(){

    lateinit var jobDetailsViewModel: JobDetailsViewModel
     var jobId:Long = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.job_details)
        jobId=intent.extras.getLong("jobId",0)
        jobDetailsViewModel = ViewModelProviders.of(this).get(JobDetailsViewModel::class.java)
        jobDetailsViewModel.jobDetailsLiveData.observe(this,object : Observer<JobDetails>{
            override fun onChanged(t: JobDetails?) {
                initViews(t!!)

            }

        })
        jobDetailsViewModel.startRequest(jobId)



    }

    private fun initViews(jobInfo: JobDetails) {
        val titleView = findViewById(R.id.job_header_title) as TextView
        titleView.setText(jobInfo?.jobTitle?.substring(0, 1).toUpperCase() + jobInfo.jobTitle.substring(1))
        val subTitleView = findViewById(R.id.job_header_sub_title) as TextView
        subTitleView.text = "Starting " + jobInfo.jobStartDate

        val slotsTitleView = findViewById(R.id.job_header_slots_title) as TextView
//        slotsTitleView.visibility = if (mJobInfo.mNumberOfBoostedApplicants < mJobInfo.mAllowableNumberOfBoostedApplicants) View.VISIBLE else View.GONE

        val addressView = findViewById(R.id.job_footer_title) as TextView
        addressView.setText(jobInfo.city + ", " + jobInfo.city + " | " + if (jobInfo.distance > 0) jobInfo.distanceText + " mi away" else "less than 1 mi")

        val rateView = findViewById(R.id.job_footer_sub_title) as TextView
//
//        if (jobInfo. > 0)
//            run {
//                rateView.text = "$" + jobInfo.mFlatRate + " Flat Rate"
//                rateView.visibility = View.VISIBLE
//            }
        rateView.setText("$" + jobInfo.hourlyRateFrom + "-" + jobInfo.hourlyRateTo + "/hr")
        rateView.setVisibility(if (jobInfo.hourlyRateFrom.toDouble() <= 0 && jobInfo.hourlyRateTo.toDouble() <= 0) View.INVISIBLE else View.VISIBLE)

        val serviceTextView = findViewById(
                R.id.job_footer_layout_item1_text) as TextView
        val serviceText = jobInfo.serviceId


        // Full Time/Part Time
        val timeView = findViewById(R.id.job_footer_layout_item2_text) as TextView
        timeView.text = if (jobInfo.isFullTime) "Full Time" else "Part Time"

        // Recurring/One time
        val recurringView = findViewById(R.id.job_footer_layout_item3_text) as TextView
        recurringView.text = if (jobInfo.jobType.equals("R")) "Recurring" else "One Time"

        // Setting Job description.
        val descriptionBlock = findViewById(R.id.job_details_description_block) as CareTextBlock
        descriptionBlock.setValue(jobInfo.description)

    }
}