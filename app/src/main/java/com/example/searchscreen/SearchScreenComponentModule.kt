package com.example.searchscreen

import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class SearchScreenComponentModule(searchParamsProviderInterface: SearchParamsProviderInterface){

    lateinit var searchParamsProviderInterface:SearchParamsProviderInterface
    init {
        this.searchParamsProviderInterface=searchParamsProviderInterface
    }
    @Provides
    public fun getListOfParams():ArrayList<Params>?{
       return searchParamsProviderInterface.getListOfParams()
    }

    @Provides
    @SearchScreenActvityScope
    public fun getJobType():SearchType{
        return SearchType.JOB_SEARCH
    }

    @Provides
    public fun getInterface():SearchParamsProviderInterface{
        return searchParamsProviderInterface
    }

    @Provides
    @Named("serverUrl")
    public fun getServerUrl():java.lang.String{
        return (java.lang.String("https://www.uat1.carezen.net/platform/spi/"))
    }

    @Provides
    public fun getSearchRequestParameters():SearchRequestParameters{
        return SearchRequestParameters()
    }
}