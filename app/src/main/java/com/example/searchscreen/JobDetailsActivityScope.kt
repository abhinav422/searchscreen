package com.example.searchscreen

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class JobDetailsActivityScope