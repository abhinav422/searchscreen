package com.example.searchscreen

enum class TypeOfCare(s: String) {
    PETCARE("PETCARE"),HOUSEKEEPING("HOUSEKEEP"),SPECIALNEEDS("SPCLNEEDS"),TUTORING("TUTORINGX")
    ,CHILDCARE("CHILDCARE"),ERRANDS_ODD_JOBS("CAREGIGSX"),SENIORCARE("SENIRCARE");
    public var value:String =s
    companion object {
        fun getEnumForName(s:String) : TypeOfCare? {
            if(s.equals("Pet Care")){
                return PETCARE
            }
            else if(s.equals("Special Needs")){
                return SPECIALNEEDS
            }
            else if(s.equals("Tutoring")){
                return TUTORING
            }
            else if(s.equals("Housekeeping")){
                return HOUSEKEEPING
            }
            else if(s.equals("Errands & Odd Jobs")){
                return ERRANDS_ODD_JOBS
            }
            else if(s.equals("Senior Care")){
                return SENIORCARE
            }
            else if(s.equals("Child Care")){
                return CHILDCARE
            }
            return null
        }
    }

}