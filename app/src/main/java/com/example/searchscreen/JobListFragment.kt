package com.example.searchscreen

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearLayoutManager.VERTICAL
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.util.*

class JobListFragment : Fragment(){

    private   var sortBy:String? = null
    private lateinit var recyclerView: RecyclerView
    private var isLoading : Boolean = false
    companion object {
        fun getInstance(sortBy : String): JobListFragment{
            var jobListFragment : JobListFragment = JobListFragment()
            var bundle : Bundle = Bundle()
            bundle.putString("sortBy",sortBy)
            jobListFragment.arguments = bundle
            return  jobListFragment
        }
    }

    private lateinit var jobCollectionAdapter: JobCollectionAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView=view.findViewById(R.id.recyclerview)
        recyclerView.layoutManager = LinearLayoutManager(context,VERTICAL,false)

        jobCollectionAdapter = JobCollectionAdapter(Hashtable(),
                object : JobCollectionAdapter.OnItemClick{
                    override fun onItemClick(jobId: Long?) {
                        var intent : Intent = Intent(activity,JobDetailsActivity::class.java)
                        intent.putExtra("jobId",jobId)
                        activity?.startActivity(intent)
                    }

                })
        recyclerView.adapter = jobCollectionAdapter
        (activity as SearchScreenActivity).searchScreenViewModel.getLiveData(sortBy!!)?.observe(this,object : Observer<Hashtable<Int, JobsList>> {
            override fun onChanged(t: Hashtable<Int, JobsList>?) {
                (activity as SearchScreenActivity).runOnUiThread(object:Runnable{
                    override fun run() {

                        if (t!!.containsKey(0) or t!!.isEmpty){
                            jobCollectionAdapter.listOfJobs?.clear()
                        }
                        jobCollectionAdapter.listOfJobs!!.putAll(t!!.toMap())
                        jobCollectionAdapter.notifyDataSetChanged()
                        isLoading = false
                    }

                })
            }
        })

        recyclerView.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                var linearLayoutManager:LinearLayoutManager =(recyclerView?.layoutManager as LinearLayoutManager)
                var totalItemCount = linearLayoutManager.itemCount
                var firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()
                var lastVisibleItemPosition = linearLayoutManager.childCount
                if(!isLoading) {
                    if (firstVisibleItemPosition + lastVisibleItemPosition >= totalItemCount) {
                        (activity as SearchScreenActivity).searchScreenViewModel.startRequest(sortBy!!, linearLayoutManager.itemCount)
                        isLoading = true
                    }
                }

            }

            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {

            }
        })



    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sortBy = arguments?.getString("sortBy")
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.job_list_fragment,container,false)
    }

    override fun onResume() {
        super.onResume()

    }
}
