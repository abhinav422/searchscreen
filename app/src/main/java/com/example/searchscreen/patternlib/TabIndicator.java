package com.example.searchscreen.patternlib;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.PaintDrawable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.OnTabSelectedListener;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import com.example.searchscreen.R;

/**
 * @author Parijat Shah
 *
 */

public class TabIndicator extends View implements OnPageChangeListener, OnTabSelectedListener {
    private int mIndicatorHeight;
    private int mIndicatorWidth;
    private int mIndicatorX;
    private int mIndicatorColor = 0xff27E4C4;
    private int mRailColor = 0xFFEEEEEE;

    private Context mContext;
    private Paint mPaintRail;
    private Paint mPaintIndicator;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private FragmentPagerAdapter mAdapter;
    private int mSelected = 0;
    private int mNextPosition = 0;
    private int mxOffset = 0;
    private int maWidths[];
    private int mDragPosition = 0;
    private float mDragOffset = 0.0f;
    private int maLocationX[];
    private PaintDrawable mPaintDrawable;

    private int mTabIndicatorWidth = 0;

    public TabIndicator(Context context) {
        super(context);
        this.mContext = context;

        createNewIndicator(context);
        //customTextView.setCompoundDrawablesRelative(null,null,drawable,null);
        Log.v("TabFactory", " set right drawable");
    }

    public TabIndicator(Context context, AttributeSet attrs) {
        super(context,attrs);
        this.mContext = context;
        createNewIndicator(context);
        TypedValue indicatorColor = new TypedValue();
        if (context.getTheme().resolveAttribute(R.attr.tabIndicatorColor, indicatorColor, true)) {
            int colorResource = indicatorColor.resourceId;
            //mIndicatorColor = context.getResources().getColor(colorResource);
        }
        mPaintRail = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintRail.setColor(mRailColor);
        mPaintIndicator = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintIndicator.setColor(mIndicatorColor);
    }
    private void createNewIndicator(Context context) {
        int size = context.getResources().getDimensionPixelSize(R.dimen.new_indicator_tab_size);
        mPaintDrawable = new PaintDrawable(0xffff0000);

        mPaintDrawable.setIntrinsicHeight(size);
        mPaintDrawable.setIntrinsicWidth(size);
        mPaintDrawable.setCornerRadius(size/2);
    }


    public void setupWithViewPager(TabLayout tabLayout, ViewPager viewPager) {
        setupWithViewPager(tabLayout,viewPager,null);
    }

    public void setupWithViewPager(TabLayout tabLayout, ViewPager viewPager, int[] icons) {
        this.mTabLayout = tabLayout;
        this.mViewPager = viewPager;

        mViewPager.addOnPageChangeListener(this);
        mAdapter = (FragmentPagerAdapter) mViewPager.getAdapter();

        int count = mAdapter.getCount();

        maWidths = new int[count];


        for(int i=0;i<count;i++) {
            TabLayout.Tab tab;
            String title = mAdapter.getPageTitle(i) != null ? mAdapter.getPageTitle(i).toString() : null;
            if(icons != null && icons.length > i) {
                tab = TabFactory.getTab(mContext,mTabLayout,title,icons[i]);
            }else {
                tab = TabFactory.getTab(mContext,mTabLayout, title);
            }
            try {
                maWidths[i]= (Integer)tab.getTag();
                if(maWidths[i] < mTabIndicatorWidth || mTabIndicatorWidth == 0) {
                    mTabIndicatorWidth = maWidths[i];
                }
            }catch(Exception e) {
            }
            mTabLayout.addTab(tab,i);
        }

        for(int i=0;i<count;i++) {
            maWidths[i] = mTabIndicatorWidth;
        }

        mSelected = mTabLayout.getSelectedTabPosition();

        final TabLayout.Tab selectedTab = mTabLayout.getTabAt(mSelected);
        final View customTabView = selectedTab.getCustomView();

        customTabView.post(new Runnable() {
            public void run() {
                if(maLocationX == null) {
                    calculateTabX();
                }
                setUpTab(mSelected);
                invalidate();
            }
        });
        mTabLayout.setOnTabSelectedListener(this);
    }

    public void setIconForTab(int tabIndex, int icon) {
        final TabLayout.Tab tab = mTabLayout.getTabAt(tabIndex);
        tab.setIcon(icon);
    }

    /**
     * Calculate indicatorX position of the indicator for each tab.
     * Please note that maWidths is the array of length of text titles of the tabs
     * Once we calculate indicatorX position for all the tabs
     */
    private void calculateTabX() {
        int count    = mAdapter.getCount();
        maLocationX  = new int[count];
        int tabWidth = mTabLayout.getMeasuredWidth()/count;

        for (int i=0; i < count; i++) {
            maLocationX[i] = (i * tabWidth) + (tabWidth - maWidths[i]) / 2;
        }
    }

    private void setUpTab(int position) {
        if(maLocationX != null)  {
            mIndicatorX = maLocationX[position];
            mxOffset = 0;
        }
        mIndicatorWidth = maWidths[position];
    }


    public void displayNewIndicator(int index,boolean bDisplay) {
        TabLayout.Tab tab = mTabLayout.getTabAt(index);
        View customTabView = tab.getCustomView();
        if(customTabView == null) return;

        View view = customTabView.findViewById(R.id.new_indicator);
        if(view == null) return;
        if(bDisplay) {
            view.setVisibility(View.VISIBLE);
        }else {
            view.setVisibility(View.INVISIBLE);
        }
    }

    public void onTabReselected(TabLayout.Tab tab) {

    }

    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(mTabLayout.getSelectedTabPosition());
        final View tabView  = tab.getCustomView();

	/*CustomTextView textView = (CustomTextView) tabView.findViewById(android.R.id.text1);
	if(textView != null) {
	    textView.setTextAppearance(R.style.SelectedTabTextStyle);
	}

	ImageView imageView = (ImageView) tabView.findViewById(android.R.id.icon);
	if(imageView != null) {
	    imageView.setSelected(true);
	    }*/
    }

    public void onTabUnselected(TabLayout.Tab tab) {
        final View customTabView = tab.getCustomView();
        CustomTextView textView = (CustomTextView) customTabView.findViewById(android.R.id.text1);

	/*if(textView != null) {
	    textView.setTextAppearance(R.style.UnselectedTabTextStyle);
	}
	ImageView imageView = (ImageView) customTabView.findViewById(android.R.id.icon);

	if(imageView != null) {
	    imageView.setSelected(true);
	    }*/
    }

    public void onPageScrollStateChanged(int state) {

        switch(state) {
            case ViewPager.SCROLL_STATE_IDLE: {
                mSelected = mTabLayout.getSelectedTabPosition();
                //setUpTab(mSelected);
                invalidate();
            }
            break;
            case ViewPager.SCROLL_STATE_DRAGGING: {

            }
            break;
            case ViewPager.SCROLL_STATE_SETTLING: {

            }
            break;

        }
    }

    /**
     * Draw tab indicator as the page in the viewpager is being dragged.
     * Indicator width also varies based on the label text of the destination tab
     *
     */
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if(positionOffset == 0) return;

        mDragPosition = position;
        mDragOffset = positionOffset;

        if(maLocationX == null) {
            calculateTabX();
        }


        //debug(" position = " + position + "position offset : " + positionOffset +  " pixelOffset = " + positionOffsetPixels);


	/*if(position >= mSelected) {
	    mIndicatorX = (int) ((maLocationX[position+1]  - maLocationX[position])  * mDragOffset);
	    mIndicatorWidth = (int ) (( maWidths[position + 1] - maWidths[position]) * mDragOffset) + maWidths[position];
	}else {
	    mIndicatorWidth = (int ) ((maWidths[position] - maWidths[mSelected]) * (1-mDragOffset)) + maWidths[mSelected];
	    mIndicatorX =  (int) -((maLocationX[mSelected] - maLocationX[position]) * (1-mDragOffset));
	    }*/


        if(position > mNextPosition) {
            mIndicatorWidth = (int ) ((maWidths[position] - maWidths[mNextPosition]) * (1-mDragOffset)) + maWidths[mSelected];
            mIndicatorX = (int) (maLocationX[mNextPosition] + (Math.abs(maLocationX[mNextPosition] - maLocationX[position]))*(1-mDragOffset));
        }else {
            mIndicatorX = (int) (maLocationX[position] + ((maLocationX[mNextPosition]  - maLocationX[position])  * mDragOffset));
            mIndicatorWidth = (int ) (( maWidths[mNextPosition] - maWidths[position]) * mDragOffset) + maWidths[position];
        }

        //debug("mIndicatorX = " + mIndicatorX);
        invalidate();
    }

    public void onPageSelected(final int position)  {
        mNextPosition = position;

        if(maLocationX == null) {

            TabLayout.Tab selectedTab = mTabLayout.getTabAt(mSelected);
            final View customTabView = selectedTab.getCustomView();

            mTabLayout.getTabAt(position).select();
            mIndicatorWidth = maWidths[position];

            selectedTab = mTabLayout.getTabAt(position);
            final View tabView 	= selectedTab.getCustomView();

            tabView.post(new Runnable() {
                public void run() {
                    calculateTabX();
                    setUpTab(position);

			/*CustomTextView textView = (CustomTextView) customTabView.findViewById(android.R.id.text1);
			if(textView != null) {
			    textView.setTextAppearance(R.style.UnselectedTabTextStyle);
			}

			textView = (CustomTextView) tabView.findViewById(android.R.id.text1);
			if(textView != null) {
			    textView.setTextAppearance(R.style.SelectedTabTextStyle);
			}
			*/
			/*if(tabView instanceof CustomTextView) {
			    CustomTextView textView = (CustomTextView) tabView;

			}
			if(customTabView instanceof CustomTextView) {
			    CustomTextView textView = (CustomTextView) customTabView;
			    textView.setTextAppearance(R.style.UnselectedTabTextStyle);
			    }*/
                }
            });
        }
        else
        {
            mTabLayout.getTabAt(position).select();
            setUpTab(position);
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(0,0,getWidth(),getHeight(),mPaintRail);

        canvas.drawRect(mIndicatorX ,0, mIndicatorX + mIndicatorWidth, getHeight(),mPaintIndicator);
        //canvas.drawRect(mIndicatorX + mxOffset,0, mxOffset+mIndicatorX + mIndicatorWidth, getHeight(),mPaintIndicator);
    }

    private void debug(String s) {
        Log.v("TabIndicator " , " " + s);
    }

}
