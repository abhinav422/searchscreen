package com.example.searchscreen.patternlib;

import android.content.Context;
import android.util.AttributeSet;

import com.example.searchscreen.R;

/**
 * CareCheckBox is a selectable row based on the style form pattern library.
 * It handles the onClick event by toggling its state but one can override the default behavior
 * by setting setOnClickListener
 */

public class CareCheckBox extends CareButton {
    public CareCheckBox(Context context) {
        super(context);
    }

    public CareCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public CareCheckBox(Context context, String sLabel, boolean state, int drawable) {
        super(context,sLabel,state,drawable);
    }

    public void setDrawable(int drawable) {
        if(drawable > 0) {
            super.setDrawable(drawable, R.style.NavigationItemImage);
        }else {
            super.setDrawable(R.drawable.checkboxpattern,R.style.NavigationItemImage);
        }
    }

}