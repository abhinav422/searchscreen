package com.example.searchscreen.patternlib;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import com.example.searchscreen.R;


/**
 * The class that creates a circular imageView
 */

public class CircularImageView extends AppCompatImageView {
    private Boolean mStrokeProperty = false;
    private int mStrokeWidth = 0;
    private int mStrokeColor = 0;

    private String mImageUrl;
    private int mStubImageId;
    private Context mContext;
    private int mSize = 0;
    private String msInitials="";
    private boolean mbDrawArc;
    private int arcSize = 0;

    private Paint mTextPaint;
    private Paint mPaintBgCircle;
    private int mLastSize = 0;

    public CircularImageView(Context context){
        super(context);
        mContext = context;

        mPaintBgCircle = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintBgCircle.setColor(0xFFEEEEEE);
        mPaintBgCircle.setStyle(Paint.Style.FILL);

        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setColor(getResources().getColor(R.color.headertext_color));
        mTextPaint.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "Lato-Bold.ttf"));

        //mTextPaint.setTextSize(getResources().getDimensionPixelSize(R.dimen.dp_big_size)  * 0.35f);
        mTextPaint.setShadowLayer(1f, 0f, 1f, Color.WHITE);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
    }

    public CircularImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CircularImageView);
        if (typedArray != null) {
            if (typedArray.hasValue(R.styleable.CircularImageView_strokeProperty)) {
                mStrokeProperty = typedArray.getBoolean(R.styleable.CircularImageView_strokeProperty,
                        mStrokeProperty);
            }
            if (mStrokeProperty) {
                if (typedArray.hasValue(R.styleable.CircularImageView_strokeWidth)) {
                    mStrokeWidth = typedArray.getDimensionPixelSize(
                            R.styleable.CircularImageView_strokeWidth, mStrokeWidth);
                }
                if (typedArray.hasValue(R.styleable.CircularImageView_strokeColor)) {
                    mStrokeColor = typedArray.getColor
                            (R.styleable.CircularImageView_strokeColor,
                                    ContextCompat.getColor(context, R.color.colorAccent));
                }
            }
            if(typedArray.hasValue(R.styleable.CircularImageView_drawArc)){
                mbDrawArc = typedArray.getBoolean(R.styleable.CircularImageView_drawArc, mbDrawArc);
            }
            typedArray.recycle();
        }

        mPaintBgCircle = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintBgCircle.setColor(0xFFEEEEEE);
        mPaintBgCircle.setStyle(Paint.Style.FILL);

        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setColor(getResources().getColor(R.color.headertext_color));
        mTextPaint.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "Lato-Bold.ttf"));

        //mTextPaint.setTextSize(getResources().getDimensionPixelSize(R.dimen.dp_big_size)  * 0.35f);
        mTextPaint.setShadowLayer(1f, 0f, 1f, Color.WHITE);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width  = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int widthWithoutPadding = width - getPaddingLeft() - getPaddingRight();
        int heigthWithoutPadding = height - getPaddingTop() - getPaddingBottom();

        // set the dimensions
        if (widthWithoutPadding > heigthWithoutPadding) {
            mSize = heigthWithoutPadding;
        } else {
            mSize = widthWithoutPadding;
        }
        setMeasuredDimension(width,height);

        if(mbDrawArc){
            //Reducing size of actual bitmap, when we have to draw the arc
            arcSize = mSize;
            mSize = mSize - dipToPixels(20);
        }

        if(msInitials != null && mLastSize != mSize) {
            mTextPaint.setTextSize(mSize  * 0.35f);
            Rect textBounds = new Rect();
            mTextPaint.getTextBounds(msInitials, 0, msInitials.length(), textBounds);
            yOffset = textBounds.height() / 2 + getPaddingTop();
        }
        mLastSize = mSize;
    }


    public void onDraw(Canvas canvas) {
        long startTime = System.currentTimeMillis();

        Bitmap bitmap = null;
        Bitmap roundedBitmap = null;
        int radius = mSize / 2;

        if(mbDrawArc){
            RectF rectF = new RectF();
            int arcRadius = arcSize/2;
            int width = getWidth();
            int height = getHeight();
            float centerX = width * 0.5F;
            float centerY = height * 0.5F;
            rectF.set(centerX-arcRadius, centerY-arcRadius, centerX+arcRadius, centerY+arcRadius);
            canvas.drawArc(rectF, 0, 180, false, getArcPaintBorder());
        }
        BitmapDrawable bitmapDrawable = null;
        try {
            bitmapDrawable = (BitmapDrawable) this.getDrawable();
        }catch (Exception e){
            if ((this.getDrawable() instanceof VectorDrawableCompat)
                    || ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) && (this.getDrawable() instanceof VectorDrawable))) {
                bitmapDrawable = convertVectorDrawableIntoBitmap((this.getDrawable()));
            }
        }


        if(bitmapDrawable == null && !msInitials.isEmpty()) {
            //super.onDraw(canvas);
            if (mbDrawArc)
            {
                canvas.drawCircle(getWidth()/2, getHeight()/2  , radius, mPaintBgCircle);
                canvas.drawText(msInitials, getWidth()/2, (getHeight()/2) + yOffset, mTextPaint);
            }
            else
            {
                canvas.drawCircle(getPaddingLeft() + radius, getPaddingTop() + radius, radius, mPaintBgCircle);
                canvas.drawText(msInitials, radius, radius + yOffset, mTextPaint);
            }
            //Log.v("CircularImageView " , " time consumed for drawing = " + (System.currentTimeMillis() - startTime) + " yOffset = " + yOffset);
            return;
        }

        if (bitmapDrawable != null) {
            bitmap = bitmapDrawable.getBitmap();
            if (bitmap != null) {
                roundedBitmap = getCircularBitmap(bitmap, radius);
            }

            if (roundedBitmap != null) {
                //padding has to be considered when drawing bitmap
		/*int widthPadding = getPaddingLeft() + getPaddingRight();
		  int heightPadding = getPaddingTop() + getPaddingBottom();
		  float left = mbDrawArc ? (mSize + widthPadding - roundedBitmap.getWidth() + dipToPixels(20)) / 2
		  : (mSize + widthPadding - roundedBitmap.getWidth()) / 2;
		  float top = mbDrawArc ? (mSize + heightPadding - roundedBitmap.getHeight() + +dipToPixels(20)) / 2
		  : (mSize + heightPadding - roundedBitmap.getHeight()) / 2; */
                if (mbDrawArc) {
                    canvas.drawBitmap(roundedBitmap, getPaddingLeft() + dipToPixels(10), getPaddingTop() + dipToPixels(10), null);
                } else {
                    canvas.drawBitmap(roundedBitmap, getPaddingLeft(), getPaddingTop(), null);
                }

            }
        }
        //xLog.v("CircularImageView " , " time consumed for long drawing = " + (System.currentTimeMillis() - startTime));
    }


    /**
     *
     * Get circular image out of bitmap given based on radius
     * @param bitmap Bitmap
     * @param radius radius of circularBitmap
     * @return bitmap  circular bitmap
     */
    private Bitmap getCircularBitmap(Bitmap bitmap, int radius) {
        Bitmap dstBitmap = null;
        int diameter = radius * 2;
        int totalStrokeWidth = 2 * mStrokeWidth;

        //reduce strokeWidth from bitmap circle radius
        int width = diameter - totalStrokeWidth;
        int height = diameter - totalStrokeWidth;

        if (width > 0 && height > 0) {
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, diameter - totalStrokeWidth,
                    diameter - totalStrokeWidth, true);

            int xCenter = scaledBitmap.getWidth() / 2;
            int yCenter = scaledBitmap.getHeight() / 2;

            dstBitmap = Bitmap.createBitmap(scaledBitmap.getWidth(), scaledBitmap.getHeight(),Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(dstBitmap);
            Paint bitmapPaint = getBitmapPaint();
            canvas.drawCircle(xCenter, yCenter,xCenter - mStrokeWidth, bitmapPaint);
            bitmapPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

            canvas.drawBitmap(scaledBitmap, 0, 0, bitmapPaint);

            if (mStrokeProperty) {
                Paint strokePaint = getStrokePaint();
                canvas.drawCircle(xCenter, yCenter,xCenter - mStrokeWidth, strokePaint);
            }

            // Likely fix for https://fabric.io/care/android/apps/com.care.android.careview/issues/5a57fde28cb3c2fa63a8457b?time=last-ninety-days
            scaledBitmap.recycle();
        }
        return dstBitmap;
    }


    /**
     *
     * Get stroke paint
     * @return Paint Paint of stroke
     */
    private Paint getStrokePaint() {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(mStrokeWidth);
        paint.setColor(mStrokeColor);
        paint.setStyle(Paint.Style.STROKE);
        return paint;
    }


    /**
     *
     * Get bitmap paint
     * @return Paint Paint of bitmap
     */
    private Paint getBitmapPaint() {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        return paint;
    }

    private Paint getArcPaintBorder(){
        Paint paintBorder = new Paint();
        paintBorder.setStrokeWidth(dipToPixels(2));
        paintBorder.setAntiAlias(true);
        paintBorder.setStyle(Paint.Style.STROKE);
        paintBorder.setColor(mStrokeColor);

        return paintBorder;
    }


    public void setDefaultProfileBitmap(String memberName){

        if (TextUtils.isEmpty(memberName)) {
            return;
        }

        if (memberName.contains(" ")) {
            String [] nameStrings = memberName.split("\\s+");

            if ((nameStrings != null) && (nameStrings.length >= 2)) {
                if ((nameStrings[0].length() >= 1) && (nameStrings[1].length() >= 1)) {
                    msInitials = nameStrings[0].substring(0,1) + nameStrings[1].substring(0,1);
                    msInitials = msInitials.toUpperCase();
                }
            }
        } else {
            msInitials = memberName.substring(0,1) + memberName.substring(0,1);
            msInitials = msInitials.toUpperCase();
        }

        super.setImageDrawable(null);

        mTextPaint.setTextSize(mSize  * 0.35f);
        Rect textBounds = new Rect();
        mTextPaint.getTextBounds(msInitials, 0, msInitials.length(), textBounds);
        yOffset = textBounds.height() / 2 + getPaddingTop();
        mLastSize = mSize;

        requestLayout();
    }

    float yOffset = 0.0f;

    public void setStroke(int strokeWidth, int strokeColor){
        mStrokeProperty = true;
        mStrokeColor = strokeColor;
        mStrokeWidth = strokeWidth;

        requestLayout();
    }

    ///
    // Convert dip to pixels. dip is used to measure device independent pixels.
    ///
    public int dipToPixels(float sizeInDips) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, sizeInDips, metrics);
    }

    public void setStrokeColor(int color){
        mStrokeColor = color;
    }

    /**
     * Method to convert VectorDrawable to a bitmap
     * @param drawable
     */
    public BitmapDrawable convertVectorDrawableIntoBitmap(Drawable drawable) {
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                drawable = (DrawableCompat.wrap(drawable)).mutate();
            }
            Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return new BitmapDrawable(bitmap);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
        return null;
    }
}

