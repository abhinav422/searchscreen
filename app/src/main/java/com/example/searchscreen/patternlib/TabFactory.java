package com.example.searchscreen.patternlib;

import android.content.Context;
import android.graphics.drawable.PaintDrawable;
import android.support.design.widget.TabLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.searchscreen.R;

/**
 * This is a factory class to create tabs based on required style using TabLayout.
 * @author Parijat Shah
 * @author Titas
 * Created on 1/21/2017
 *
 */


public class TabFactory {
    ///
    //Data members
    ///
    private static int tabWithLabelStyle             = R.style.UnselectedTabTextStyle;
    private static int tabWithIconStyle              = R.style.TabWithIcon;
    private static int tabWithIconAndLabelStyle      = R.style.TabWithIconAndLabelStyle;
    private PaintDrawable mPaintDrawable;



    public static TabLayout.Tab getTab(Context context, TabLayout tabLayout, String title) {
        LinearLayout view = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.texttab,null);

        CustomTextView customTextView = (CustomTextView) view.findViewById(android.R.id.text1);
        int width = 0;

        if(title != null && !TextUtils.isEmpty(title) && customTextView != null) {
            TextPaint paint = customTextView.getPaint();
            width = (int)paint.measureText(title);
        }

        TabLayout.Tab tab = tabLayout.newTab().setCustomView(view);
        tab.setText(title);
        tab.setTag(width);
        return tab;
    }

    public static TabLayout.Tab getTab(Context context, TabLayout tabLayout, String title, int icon) {
        return getTab(context,tabLayout,title,icon,R.layout.tab);
    }

    public static TabLayout.Tab getTab(Context context, TabLayout tabLayout, String title, int icon, int tabL) {

        LinearLayout view = (LinearLayout) LayoutInflater.from(context).inflate(tabL,null);
        CustomTextView customTextView = (CustomTextView) view.findViewById(android.R.id.text1);

        int width = 0;

        if(title != null && !TextUtils.isEmpty(title) && customTextView != null) {
            TextPaint paint = customTextView.getPaint();
            width = (int)paint.measureText(title);
        }else{
            customTextView.setVisibility(View.GONE);
            //Setting a default width for indicator, when no title is there for tab
            width = context.getResources().getDimensionPixelSize(R.dimen.padding_fifty);
        }



        TabLayout.Tab tab = tabLayout.newTab().setCustomView(view);
        if(title != null && !TextUtils.isEmpty(title)) {
            tab.setText(title);
        }
        tab.setIcon(icon);
        tab.setTag(width);
        return tab;
    }

    /*
     * Helper method to  get custom tab with label
     * Returns a custom text view to be added as a tab
     */
    public static View getTabWithLabel(Context context, String title, int labelStyle)
    {
        int style = (labelStyle > 0)?labelStyle:tabWithLabelStyle;
        ContextThemeWrapper customContext = new ContextThemeWrapper(context, style);
        CustomTextView customTextView = new CustomTextView(customContext);

        if(!TextUtils.isEmpty(title)) {
            customTextView.setText(title);
        }
        return customTextView;
    }

    /*
     *Helper method to get custom tab with an icon
     */
    public static View getTabWithIcon(Context context, int iconStyleId)
    {
        int style = (iconStyleId > 0)?iconStyleId:tabWithIconStyle;
        ContextThemeWrapper customContext = new ContextThemeWrapper(context, style);
        ImageView customImageView = new ImageView(customContext, null, 0);
        return customImageView;
    }

    /*
     *Helper method to get custom tab with icon and label
     */
    public static View getTabWithIconAndLabel(Context context, int styleId, String title)
    {
        int style = (styleId > 0)?styleId:tabWithIconAndLabelStyle;
        ContextThemeWrapper customContext = new ContextThemeWrapper(context, style);
        CustomTextView customTextView = new CustomTextView(customContext);
        customTextView.setText(title);
        return customTextView;
    }


    private static void debug(String s) {
        Log.v("TabFactory " , " " + s);
    }

}

