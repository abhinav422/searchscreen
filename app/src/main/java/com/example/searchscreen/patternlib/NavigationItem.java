package com.example.searchscreen.patternlib;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.searchscreen.R;

import java.lang.reflect.Method;

/*
 * This class is represents a single row with maximum three elements. The elements are positioned at left, center, and right.
 * The center element is aligned to left by default but it can be aligned center by using custsom style
 * NavigationItemText.Center
 * An element can be either text, image or a composite view
 *
 */
public class NavigationItem extends ConstraintLayout {

    /*
     * The left element
     */
    protected View mViewL;
    /*
     * The right element
     */
    protected View mViewR;
    /*
     * The center element
     */
    protected View mViewC;


    protected Context mContext;
    /*
     * Paint used to draw divider
     */
    private Paint mPaintDivider;

    /*
     * The method of the activity ( dereferenced from mContext ) to be called
     * when row is clicked
     */
    private String msOnClick;

    /*
     * The method to be called when left element is clicked
     */
    private String msOnLeftClick;

    /*
     * The method to be called when right element is clicked
     */
    private String msOnRightClick;

    /*
     * The method to be called when center element is clicked
     */
    private String msOnCenterClick;

    private int RIGHT = 0;
    private int CENTER = 0;
    private int LEFT = 0;
    /**
     * @param context Context can be base context or it can wrap base context and a style / theme
     *        to be applied to the conext.
     *
     */
    public NavigationItem(Context context) {
        super(context);

        if(context instanceof ContextThemeWrapper) {
            context = ((ContextThemeWrapper)context).getBaseContext();
        }
        this.mContext = context;

        RIGHT  = getContext().getResources().getInteger(R.integer.right);
        CENTER  = getContext().getResources().getInteger(R.integer.center);
        LEFT = getContext().getResources().getInteger(R.integer.left);

    }

    /**
     * @param context
     * @param attrs
     * construct row from attributeset. The @param context is always the base context
     * Check for the presence of left, center and right elements. If present, create relavent views by applying
     * custom style if supplied or default style if not supplied.
     */

    public NavigationItem(Context context, AttributeSet attrs) {
        super(context,attrs);
        if(context instanceof ContextThemeWrapper) {
            this.mContext = ((ContextThemeWrapper)context).getBaseContext();
        }else {
            this.mContext = context;
        }

        RIGHT  = getContext().getResources().getInteger(R.integer.right);
        CENTER  = getContext().getResources().getInteger(R.integer.center);
        LEFT = getContext().getResources().getInteger(R.integer.left);

        if(getId() == -1) {
            setId(R.id.parent_view);
        }

        TypedArray a = mContext.obtainStyledAttributes(attrs,R.styleable.NavigationItem);

        if(a.hasValue(R.styleable.NavigationItem_left)) {
            int styleId = -1;
            if(a.hasValue(R.styleable.NavigationItem_leftstyle)) {
                styleId = a.getResourceId(R.styleable.NavigationItem_leftstyle,-1);
            }
            createAndAddView(mContext,a,R.styleable.NavigationItem_left,RelativeLayout.ALIGN_PARENT_LEFT,styleId);
            //mViewL.setId(R.id.left_view);
        }

        if(a.hasValue(R.styleable.NavigationItem_right)) {
            int styleId = -1;
            if(a.hasValue(R.styleable.NavigationItem_rightstyle)) {
                styleId = a.getResourceId(R.styleable.NavigationItem_rightstyle,-1);
            }
            createAndAddView(mContext,a,R.styleable.NavigationItem_right,RelativeLayout.ALIGN_PARENT_RIGHT,styleId);
            //mViewR.setId(R.id.right_view);
        }


        if(a.hasValue(R.styleable.NavigationItem_center)) {
            int styleId = -1;
            int pos = -1;

            if(a.hasValue(R.styleable.NavigationItem_centerstyle)) {
                styleId = a.getResourceId(R.styleable.NavigationItem_centerstyle,-1);
            }

            if(a.hasValue(R.styleable.NavigationItem_centergravity)) {
                pos = a.getInt(R.styleable.NavigationItem_centergravity,pos);
                if( pos != RIGHT && pos != CENTER) {
                    pos = -1;
                }
            }

            createAndAddView(mContext,a,R.styleable.NavigationItem_center,pos,styleId);
            //mViewC.setId(R.id.center_view);
        }

        if(a.hasValue(R.styleable.NavigationItem_ni_divider)) {
            mPaintDivider = new Paint(Paint.ANTI_ALIAS_FLAG);
            mPaintDivider.setStyle(Paint.Style.FILL);
            setWillNotDraw(false);

            if (a.getBoolean(R.styleable.NavigationItem_ni_divider, false)) {
                mPaintDivider.setColor(0xFFBCBCBC);
            } else {
                int color = a.getColor(R.styleable.NavigationItem_ni_divider, 0x00000000);
                mPaintDivider.setColor(color);
            }
        }

        if(a.hasValue(R.styleable.NavigationItem_onClick)) {
            setClickable(true);
            msOnClick = a.getString(R.styleable.NavigationItem_onClick);
            this.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    try {
                        Method method = ((AppCompatActivity)mContext).getClass().getMethod(msOnClick,View.class);
                        method.invoke(mContext,view);
                    }catch(Exception e) {
                    }
                }
            });
        }


        if(a.hasValue(R.styleable.NavigationItem_onLeftClick)) {
            if(mViewL != null) {
                msOnLeftClick = a.getString(R.styleable.NavigationItem_onLeftClick);
                mViewL.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        try {
                            Method method = ((AppCompatActivity)mContext).getClass().getMethod(msOnLeftClick,View.class);
                            method.invoke(mContext,view);
                        }catch(Exception e) {
                        }
                    }
                });
            }
        }

        if(a.hasValue(R.styleable.NavigationItem_onRightClick)) {
            if(mViewR != null) {
                msOnRightClick = a.getString(R.styleable.NavigationItem_onRightClick);
                mViewR.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        try {
                            Method method = ((AppCompatActivity)mContext).getClass().getMethod(msOnRightClick,View.class);
                            method.invoke(mContext,view);
                        }catch(Exception e) {
                        }
                    }
                });
            }
        }

        if(a.hasValue(R.styleable.NavigationItem_onCenterClick)) {
            if(mViewC != null) {

                msOnCenterClick = a.getString(R.styleable.NavigationItem_onCenterClick);
                mViewC.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        try {
                            Method method = ((AppCompatActivity)mContext).getClass().getMethod(msOnCenterClick,View.class);
                            method.invoke(mContext,view);
                        }catch(Exception e) {
                        }
                    }
                });
            }
        }
        a.recycle();
        mContext = getContext();
    }


    /**
     * Helper method to create view element by applying supplied style or default style and adding it
     * as child to the Navigation Item ( row )
     *
     */
    private void createAndAddView(Context context,TypedArray a,int index,int posRule, int styleId) {
        ImageView imageView = null;
        CustomTextView textView = null;
        String sExtention = null;
        ContextThemeWrapper customContext = null;

        String s  = a.getString(index) ;
        if(s.indexOf(".") >= 0) {
            sExtention = s.substring(s.lastIndexOf("."));
        }

        if(sExtention != null && (sExtention.toLowerCase().equals(".jpg") || sExtention.toLowerCase().equals(".png") || sExtention.toLowerCase().equals(".xml"))) {
            try {
                Drawable d = a.getDrawable(index);
                if(styleId <= 0) {
                    styleId = R.style.NavigationItemImage;
                }
                customContext = new ContextThemeWrapper(context,styleId);
                imageView = new ImageView(customContext);
                imageView.setAdjustViewBounds(true);
                imageView.setImageDrawable(d);
                ConstraintLayout.LayoutParams lp = new ConstraintLayout.LayoutParams(customContext,null);
                //addView(imageView);

                setPosition(context,lp,posRule,imageView);

                //lp.addRule(RelativeLayout.CENTER_VERTICAL);
            }catch(Exception e) {
            }
        }

        if(imageView == null) {
            String textWeight = "regular";
            if(a.hasValue(R.styleable.NavigationItem_textWeight)) {
                if(a.getType(R.styleable.NavigationItem_textWeight) == TypedValue.TYPE_STRING) {
                    try {
                        textWeight = a.getString(R.styleable.NavigationItem_textWeight).toLowerCase();
                    }catch(Exception e) {
                    }
                }
            }

            if(styleId > 0) {
                customContext = new ContextThemeWrapper(context,styleId);
            }else {
                customContext = new ContextThemeWrapper(context,R.style.NavigationItemText);
            }

            textView = new CustomTextView(customContext,textWeight);
            textView.setText(s);
            ConstraintLayout.LayoutParams lp = new ConstraintLayout.LayoutParams(customContext, null);
            textView.setLayoutParams(lp);

            textView.setTextAppearance(styleId);
            setPosition(context,lp,posRule,textView);

	    /*if(mViewL != null && mViewL instanceof TextView && textView == mViewC) {
	      lp.addRule(RelativeLayout.CENTER_VERTICAL);
	      // @todo align secondary text to the bottom of primary text
	      }else {
	      lp.addRule(RelativeLayout.CENTER_VERTICAL);
	      }*/


            //addView(textView);
        }
    }

    /**
     * Helper method to set the position of the element in the row layout
     * The position is one of the left, center and right
     * if the element is to be position centered, left and right padding is added to the center element to visually
     * distinguish it from left and right elements.
     */

    private void setPosition(Context context,ConstraintLayout.LayoutParams lp, int posRule, View view) {
        int padding = context.getResources().getDimensionPixelSize(R.dimen.navigation_item_padding);
        ConstraintSet set = new ConstraintSet();



        if(posRule == RelativeLayout.ALIGN_PARENT_LEFT) {

            if (mViewL != null)
            {
                removeView(mViewL);
            }

            mViewL = view;
            mViewL.setId(R.id.left_view);
            mViewL.setLayoutParams(lp);
            addView(mViewL);

            set.clone(this);

            set.connect(R.id.left_view,ConstraintSet.LEFT,ConstraintSet.PARENT_ID,ConstraintSet.LEFT,0);

            set.centerVertically(R.id.left_view,ConstraintSet.PARENT_ID);
            set.setHorizontalChainStyle(R.id.left_view, ConstraintSet.CHAIN_SPREAD_INSIDE);
            set.applyTo(this);
        }else if(posRule == RelativeLayout.ALIGN_PARENT_RIGHT) {

            if (mViewR != null)
            {
                removeView(mViewR);
            }
            mViewR = view;
            mViewR.setId(R.id.right_view);

            mViewR.setLayoutParams(lp);
            addView(mViewR);
            set.clone(this);

            if(mViewL != null) {
                set.connect(R.id.left_view,ConstraintSet.RIGHT,R.id.right_view,ConstraintSet.LEFT,0);
                set.connect(R.id.right_view,ConstraintSet.LEFT,R.id.left_view,ConstraintSet.RIGHT,0);
            }

            set.connect(R.id.right_view,ConstraintSet.RIGHT,ConstraintSet.PARENT_ID,ConstraintSet.RIGHT,0);
            set.centerVertically(R.id.right_view,ConstraintSet.PARENT_ID);
            set.applyTo(this);
        }else if(posRule == -1 || posRule == RIGHT || posRule == CENTER || posRule == LEFT) {

            if (mViewC != null)
            {
                removeView(mViewC);
            }

            mViewC = view;
            mViewC.setId(R.id.center_view);
            mViewC.setLayoutParams(lp);
            addView(mViewC);
            set.clone(this);

            if(mViewL != null && mViewR != null) {
                if(posRule == RIGHT) {
                    set.connect(R.id.center_view,ConstraintSet.RIGHT,R.id.right_view,ConstraintSet.LEFT,padding);
                }else if(posRule == CENTER ) {
                    debug(" center view");
                    set.centerHorizontally(R.id.center_view,ConstraintSet.PARENT_ID);
                }else if(posRule == LEFT) {
                    set.connect(R.id.center_view,ConstraintSet.LEFT,R.id.left_view,ConstraintSet.RIGHT,0);
                }else {
                    if(mViewL != null && mViewL instanceof TextView && mViewC instanceof TextView) {
                        mViewC.setPadding(padding/2,0,padding,0);
                    }else {
                        mViewC.setPadding(padding,0,padding,0);
                    }
                    set.connect(R.id.center_view,ConstraintSet.RIGHT,R.id.right_view,ConstraintSet.LEFT,0);
                    set.connect(R.id.center_view,ConstraintSet.LEFT,R.id.left_view,ConstraintSet.RIGHT,0);
                    set.constrainWidth(R.id.center_view,ConstraintSet.MATCH_CONSTRAINT);
                }

                if(mViewL != null && mViewL instanceof TextView && mViewC instanceof TextView) {
                    set.connect(R.id.center_view,ConstraintSet.BASELINE,R.id.left_view,ConstraintSet.BASELINE,0);
                }else {
                    set.centerVertically(R.id.center_view,ConstraintSet.PARENT_ID);
                }
            }else if(mViewL == null && mViewR != null) {
                mViewC.setPadding(padding,0,padding,0);
                if(posRule == RIGHT) {
                    mViewC.setPadding(padding,0,padding,0);
                    set.connect(R.id.center_view,ConstraintSet.RIGHT,R.id.right_view,ConstraintSet.LEFT,padding);
                }else {
                    //set.centerHorizontally(R.id.center_view,ConstraintSet.PARENT_ID);
                    //set.connect(R.id.center_view,ConstraintSet.LEFT,R.id.right_view,ConstraintSet.LEFT,padding);
                }

                if(mViewL != null && mViewL instanceof TextView && mViewC instanceof TextView) {
                    set.connect(R.id.center_view,ConstraintSet.BASELINE,R.id.left_view,ConstraintSet.BASELINE,0);
                }else {
                    set.centerVertically(R.id.center_view,ConstraintSet.PARENT_ID);
                }

            }else if(mViewL != null && mViewR == null) {

                if(posRule == CENTER) {
                    set.centerHorizontally(R.id.center_view,ConstraintSet.PARENT_ID);
                }else {
                    //set.centerHorizontally(R.id.center_view,ConstraintSet.PARENT_ID);
                    set.connect(R.id.center_view,ConstraintSet.LEFT,R.id.left_view,ConstraintSet.RIGHT,0);
                }

                if(mViewL != null && mViewL instanceof TextView && mViewC instanceof TextView) {
                    mViewC.setPadding(padding/2,0,padding,0);
                    set.connect(R.id.center_view,ConstraintSet.BASELINE,R.id.left_view,ConstraintSet.BASELINE,0);
                }else {
                    mViewC.setPadding(padding,0,padding,0);
                    set.centerVertically(R.id.center_view,ConstraintSet.PARENT_ID);
                }
            }

            set.applyTo(this);
	    /*if(mViewL != null && mViewR != null) {
		lp.addRule(LEFT_OF,R.id.right_view);
		lp.addRule(RIGHT_OF,R.id.left_view);

		if(posRule == LEFT) {
		    lp.addRule(START_OF,R.id.left_view);
		}else if(posRule == RIGHT) {
		    lp.addRule(START_OF,R.id.right_view);
		}

		if(mViewL != null && mViewL instanceof TextView && view instanceof TextView) {
		    mViewC.setPadding(padding/2,0,padding,0);
		}else {
		    mViewC.setPadding(padding,0,padding,0);
		}

	    }else if(mViewL == null && mViewR != null) {
		lp.addRule(LEFT_OF,R.id.right_view);
		mViewC.setPadding(0,0,padding,0);
	    }else if(mViewL != null && mViewR == null) {
		lp.addRule(RIGHT_OF,R.id.left_view);
		if(mViewL != null && mViewL instanceof TextView && view instanceof TextView) {
		    mViewC.setPadding(padding/2,0,0,0);
		}else {
		    mViewC.setPadding(padding,0,0,0);
		}
	    }else if(mViewL == null && mViewR == null) {

	    }*/
        }
        //lp.addRule(RelativeLayout.CENTER_VERTICAL);
    }


    private void debug(String s) {
        Log.v("NavigationItem ", " " + s);
    }

    /**
     * Programatically create Navigation Item with default style
     */
    public static NavigationItem build(Context context) {
        if(context instanceof ContextThemeWrapper) {
            context = ((ContextThemeWrapper)context).getBaseContext();
        }
        ContextThemeWrapper customContext = new ContextThemeWrapper(context,R.style.NavigationItem);
        return new NavigationItem(customContext);
    }

    /**
     * Programatically create Navigation Item with supplied style
     */

    public static NavigationItem build(Context context, int style) {
        ContextThemeWrapper customContext = new ContextThemeWrapper(context,style);
        return new NavigationItem(customContext);
    }

    /**
     * @param pos the position of the textview in the row
     * @style the style to be applied to the textview
     */
    private TextView createTextView(int pos, int style) {
        ContextThemeWrapper context = null;
        if(style > 0) {
            context = new ContextThemeWrapper(mContext,style);
        }else {
            context = new ContextThemeWrapper(mContext,R.style.NavigationItemText);
        }
        CustomTextView textView = new CustomTextView(context);
        ConstraintLayout.LayoutParams lp;
        //Fix for CAREUS-41472: Plib PAJ: Few UI issues
        if(style == R.style.NavigationItemText_MaxWidth)
        {
            DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
            int width = (int) getResources().getDimension(R.dimen.navigation_item_text_max_width);
            lp = new ConstraintLayout.LayoutParams((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width, metrics),ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        else
        {
            lp = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        //textView.setLayoutParams(lp);

        if(style > 0) {
            textView.setTextAppearance(context,style);
        }else {
            textView.setTextAppearance(context, R.style.NavigationItemText);
        }
        setPosition(context,lp,pos,textView);

        //addView(textView);
        return textView;
    }

    /**
     * @param pos the position of the imageview in the row
     * @style the style to be applied to the imageview
     */
    private ImageView createImageView(int pos, int style) {
        ContextThemeWrapper context = null;
        if(style > 0) {
            context = new ContextThemeWrapper(mContext,style);
            TypedArray ta = context.obtainStyledAttributes(style, R.styleable.NavigationItem);

            pos = ta.getInteger(R.styleable.NavigationItem_centergravity,pos);
            debug("setPosition = " + " posRule=" + pos );
            ta.recycle();
        }else {
            context = new ContextThemeWrapper(mContext,R.style.NavigationItemImage);
        }

        ImageView imageView = new ImageView(context);
        ConstraintLayout.LayoutParams lp = new ConstraintLayout.LayoutParams(context,null);
        imageView.setAdjustViewBounds(true);
        //imageView.setLayoutParams(lp);

        setPosition(context,lp,pos,imageView);
        //addView(imageView);

        return imageView;
    }

    /**
     * @param show if true draw divider
     * programatically set if divider should be drawn or not
     *
     */
    public NavigationItem setDivider(boolean show) {
        setWillNotDraw(false);
        mPaintDivider = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintDivider.setStyle(Paint.Style.FILL);
        if(show) {
            mPaintDivider.setColor(0xFFBCBCBC);
        }
        else
        {
            mPaintDivider.setColor(0x00000000);
        }
        return this;
    }

    /**
     * @param color
     * Draw divider of the color
     */
    public NavigationItem setDivider(int color) {
        setWillNotDraw(false);
        mPaintDivider = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintDivider.setStyle(Paint.Style.FILL);
        mPaintDivider.setColor(color);
        invalidate();
        return this;
    }

    public NavigationItem setLeftView(int resource,int style) {
        String resourceType = mContext.getResources().getResourceTypeName(resource);
        if(resourceType.equals("string")) {
            TextView textView = createTextView(RelativeLayout.ALIGN_PARENT_LEFT,style);
            textView.setText(resource);
            textView.setId(R.id.left_view);
        }else if(resourceType.equals("drawable")) {
            ImageView imageView = createImageView(RelativeLayout.ALIGN_PARENT_LEFT,style);
            imageView.setImageResource(resource);
            imageView.setId(R.id.left_view);
        }
        return this;
    }

    public NavigationItem setLeftView(String s,int style) {
        TextView textView = createTextView(RelativeLayout.ALIGN_PARENT_LEFT,style);
        textView.setText(s);
        textView.setId(R.id.left_view);
        return this;
    }

    public NavigationItem setLeftView(Bitmap b, int style) {
        ImageView imageView = createImageView(RelativeLayout.ALIGN_PARENT_LEFT,style);
        imageView.setImageBitmap(b);
        imageView.setId(R.id.left_view);
        return this;
    }

    public NavigationItem setLeftView(int resource) {
        return setLeftView(resource,0);
    }
    public NavigationItem setLeftView(String s) {
        return setLeftView(s,0);
    }
    public NavigationItem setLeftView(Bitmap b) {
        return setLeftView(b,0);
    }

    public NavigationItem setLeftView(View view) {
        ConstraintLayout.LayoutParams lp = new ConstraintLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        setPosition(mContext, lp, RelativeLayout.ALIGN_PARENT_LEFT, view);
        return this;
    }

    public NavigationItem setLeftView(View view, ConstraintLayout.LayoutParams lp){
        if(lp == null){
            setLeftView(view);
        }
        setPosition(mContext, lp, RelativeLayout.ALIGN_PARENT_LEFT, view);
        return this;
    }


    public NavigationItem setRightView(int resource,int style) {
        String resourceType = mContext.getResources().getResourceTypeName(resource);
        if(resourceType.equals("string")) {
            TextView textView = createTextView(RelativeLayout.ALIGN_PARENT_RIGHT,style);
            textView.setText(resource);
            textView.setId(R.id.right_view);
        }else if(resourceType.equals("drawable")) {
            ImageView imageView = createImageView(RelativeLayout.ALIGN_PARENT_RIGHT,style);
            imageView.setImageResource(resource);
            imageView.setId(R.id.right_view);
        }
        return this;
    }

    public NavigationItem setRightView(String s,int style) {
        TextView textView = createTextView(RelativeLayout.ALIGN_PARENT_RIGHT, style);
        textView.setText(s);
        textView.setId(R.id.right_view);
        return this;
    }

    public NavigationItem setRightView(Bitmap b, int style) {
        ImageView imageView = createImageView(RelativeLayout.ALIGN_PARENT_RIGHT, style);
        imageView.setImageBitmap(b);
        imageView.setId(R.id.right_view);

        return this;
    }

    public NavigationItem setRightView(View view) {
	/*view.setId(R.id.right_view);
	addView(view);
	mViewR = view;
	*/
	/*RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
	lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
	mViewR.setLayoutParams(lp);*/

        ConstraintLayout.LayoutParams lp = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        setPosition(mContext, lp, RelativeLayout.ALIGN_PARENT_RIGHT, view);

        return this;
    }

    public NavigationItem setRightView(int resource) {
        return setRightView(resource,0);
    }
    public NavigationItem setRightView(String s) {
        return setRightView(s,0);
    }
    public NavigationItem setRightView(Bitmap b) {
        return setRightView(b,0);
    }


    public NavigationItem setCenterView(int resource,int style) {
        String resourceType = mContext.getResources().getResourceTypeName(resource);
        if(resourceType.equals("string")) {
            TextView textView = createTextView(-1,style);
            textView.setText(resource);
            textView.setId(R.id.center_view);
        }else if(resourceType.equals("drawable")) {
            ImageView imageView = createImageView(-1,style);
            imageView.setImageResource(resource);
            imageView.setId(R.id.center_view);
        }
        return this;
    }

    public NavigationItem setCenterView(SpannableString s) {
        CustomTextView textView 		 = new CustomTextView(mContext);
        ConstraintLayout.LayoutParams lp = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);

        setPosition(mContext, lp, -1, textView);

        textView.setText(s);
        textView.setId(R.id.center_view);
        mViewC = textView;

        return this;
    }

    public NavigationItem setCenterView(String s, int style) {
        TextView textView = createTextView(-1, style);
        textView.setText(s);
        textView.setId(R.id.center_view);
        mViewC = textView;
        return this;


    }

    public NavigationItem setCenterView(Bitmap b, int style) {
        ImageView imageView = createImageView(-1,style);
        imageView.setImageBitmap(b);
        mViewR = imageView;
        return this;
    }

    public NavigationItem setCenterView(View view) {
        ConstraintLayout.LayoutParams lp = new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        setPosition(mContext, lp, RIGHT, view);
        return this;
    }

    public NavigationItem setCenterView(int resource) {
        return setCenterView(resource,0);
    }
    public NavigationItem setCenterView(String s) {
        return setCenterView(s,0);
    }
    public NavigationItem setCenterView(Bitmap b) {
        return setCenterView(b,0);
    }

    public void setOnCenterViewClickListener(OnClickListener onClickListener) {
        mViewC.setOnClickListener(onClickListener);
    }

    public void setOnRightViewClickListener(OnClickListener onClickListener) {
        if(mViewR != null && onClickListener!= null) {
            mViewR.setOnClickListener(onClickListener);
        }
    }

    /**
     * @param canvas
     * override onDraw only when divider needs to be drawn
     */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(mPaintDivider != null) {
            canvas.drawRect(0,getHeight()- dp2px(1) ,getWidth(),getHeight(),mPaintDivider);
        }
    }

    /**
     * @return mViewR
     */

    public View getRightView() {
        return mViewR;
    }

    /**
     * @return mViewL;
     */
    public View getLeftView() {
        return mViewL;
    }

    /**
     * @return mViewC;
     */
    public View getCenterView() {
        return mViewC;
    }

    public  int dp2px(float dp) {
        Resources r = mContext.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        return Math.round(px);
    }
}


/*if(styleId > 0) {
//int style = a.getResourceId(R.styleable.NavigationItem_customTextStyle,0);
textView.setTextAppearance(context,styleId);
}else {
textView.setTextAppearance(context, R.style.NavigationItemText);
}
*/

/*DisplayMetrics metrics = context.getResources().getDisplayMetrics();
	  debug("Device density = " + metrics.density); */
