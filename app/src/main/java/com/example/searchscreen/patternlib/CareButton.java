package com.example.searchscreen.patternlib;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.View;

import com.example.searchscreen.R;

/**
 * This class represents a two state button.
 * It handles the click by default by toggling its state but one can override the default behavior
 * by setting setOnClickListener
 */

public abstract class CareButton extends NavigationItem {
    protected boolean mbState = false;

    protected View.OnClickListener mClickHandler = new View.OnClickListener() {
        public void onClick(View view) {
            mbState = !mbState;
            mViewR.setSelected(mbState);
        }
    };

    protected CareButton(Context context) {
        super(context);
        setDrawable(0);
    }

    /**
     * @param context
     * @param attrs
     * Creates a on/off button
     * Please note that the label for the button will have to be set explicitly after
     * the button is created by calling @see setLeftView(String s) or @see setLeftView(String s, int style)
     * The default state of the button is false
     *
     */

    protected CareButton(Context context, AttributeSet attrs) {
        super(context,attrs);
        TypedArray a = context.obtainStyledAttributes(attrs,R.styleable.NavigationItem);
        if(a.hasValue(R.styleable.NavigationItem_defaultState)) {
            mbState = a.getBoolean(R.styleable.NavigationItem_defaultState,mbState);
        }
        a.recycle();
        setDrawable(0);
    }

    /**
     * @param context
     * @param sLabel the lable for the radio button
     * @param state the initial state of the radio button
     * @param drawable the state drawable to be used with radio button
     */
    public CareButton(Context context, String sLabel, boolean state, int drawable) {
        super(new ContextThemeWrapper(context, R.style.NavigationItem));
        mbState = state;
        setLeftView(sLabel,R.style.NavigationItemText);
        setDrawable(drawable);
    }

    public abstract void setDrawable(int drawable);

    public void setDrawable(int drawable, int style) {
        setRightView(drawable,style);

        if(mViewR != null) {
            mViewR.setSelected(mbState);
            mViewR.setClickable(false);
        }
        setClickable(true);
        setOnClickListener(mClickHandler);
    }

    /**
     * @param state the state of the radio button true or false
     */

    public void setState(boolean state) {
        mbState = state;
        if(mViewR != null) {
            mViewR.setSelected(mbState);
        }
    }
    /**
     * @return mbState the current state of the radio button
     */
    public boolean getState() {
        return mbState;
    }

    /**
     * @param listener
     *
     */

    public void setOnClickListener(View.OnClickListener listener) {
	/*if(mViewR != null) {
	  mViewR.setClickable(false);
	  }*/
        super.setOnClickListener(listener);
    }

}