package com.example.searchscreen.patternlib;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;


public class ScaledImageView extends ImageView {
    public ScaledImageView(Context context) {
        super(context);
    }

    public ScaledImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Drawable mDrawable = getDrawable();
        if (mDrawable == null) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }

        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int width = heightSize * mDrawable.getIntrinsicWidth() / mDrawable.getIntrinsicHeight();

        setMeasuredDimension(width, heightSize);

    }

    private void debug(String s) {
        //Log.v("ScaledImageView " , " " + s);
    }

}