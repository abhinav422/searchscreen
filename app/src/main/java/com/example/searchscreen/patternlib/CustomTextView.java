package com.example.searchscreen.patternlib;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;

import com.example.searchscreen.R;

public class CustomTextView extends TextView {
    /**
     * When all activities are killed this will be still in memory
     * unless ofcourse you deinit it.
     * Note that it uses applicationContext, if we use activity context instead
     * we will leak the activity when its destroyed.
     * It is not done on June 29, 2017
     */

    static Typeface LATO_BOLD;
    static Typeface LATO_HEAVY;
    static Typeface LATO_SEMI_BOLD;
    static Typeface LATO_THIN;
    static Typeface LATO_HAIRLINE;
    static Typeface LATO_MEDIUM;
    static Typeface LATO_REGULAR;
    static Typeface LATO_ITALIC;

    private Context mContext;
    private boolean mScale;

    private void debug(String s) {
        //Log.v("CustomTextView " , " " + s);
    }

    public static void initializeFonts(Context applicationContext) {
        if (LATO_BOLD != null) return;
        try {
            LATO_BOLD = Typeface.createFromAsset(applicationContext.getAssets(), "Lato-Bold.ttf");
            LATO_HEAVY = Typeface.createFromAsset(applicationContext.getAssets(), "Lato-Heavy.ttf");
            LATO_SEMI_BOLD = Typeface.createFromAsset(applicationContext.getAssets(), "Lato-Semibold.ttf");
            LATO_THIN = Typeface.createFromAsset(applicationContext.getAssets(), "Lato-Thin.ttf");
            LATO_HAIRLINE = Typeface.createFromAsset(applicationContext.getAssets(), "Lato-Hairline.ttf");
            LATO_MEDIUM = Typeface.createFromAsset(applicationContext.getAssets(), "Lato-Medium.ttf");
            LATO_ITALIC = Typeface.createFromAsset(applicationContext.getAssets(), "Lato-Italic.ttf");
            LATO_REGULAR = Typeface.createFromAsset(applicationContext.getAssets(), "Lato.ttf");
        } catch (Exception e) {
            Log.v("CustomTextView ", " " + e.toString());
        }
    }

    public CustomTextView(Context context) {
        super(context);
        this.mContext = context;
        if (context instanceof ContextThemeWrapper) {
            TypedValue textWeightValue = new TypedValue();
            if (context.getTheme().resolveAttribute(R.attr.textWeight, textWeightValue, true)) {
                setCustomTypeface(textWeightValue.string.toString());
                return;
            }
        }
        setCustomTypeface("regular");
    }

    public CustomTextView(Context context, String textWeight) {
        super(context);
        this.mContext = context;
        setCustomTypeface(textWeight);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.NavigationItem);

        if (a.hasValue(R.styleable.NavigationItem_textWeight)) {
            try {
                String textWeight = a.getString(R.styleable.NavigationItem_textWeight).toLowerCase();
                setCustomTypeface(textWeight);
            } catch (Exception e) {
            }
        } else {
            setCustomTypeface("regular");
        }

        mScale = a.getBoolean(R.styleable.NavigationItem_scale, false);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * This is the evil piece of code that was getting executed every time,
     * textView style was changed slowing down UI
     * <p>
     * try {
     * Typeface typeface = Typeface.createFromAsset(context.getAssets(),sFile);
     * <p>
     * }catch(Exception e) {
     * Log.v("CustomTextView " , " " + e.toString());
     * }
     */
    private void setCustomTypeface(String textWeight) {
        //String sFile = "Lato.ttf";
        Typeface typeface;

        if (textWeight.equals("bold")) {
            //sFile = "Lato-Bold.ttf";
            typeface = LATO_BOLD;
        } else if (textWeight.equals("heavy")) {
            //sFile = "Lato-Heavy.ttf";
            typeface = LATO_HEAVY;
        } else if (textWeight.equals("semibold")) {
            //sFile = "Lato-Semibold.ttf";
            typeface = LATO_SEMI_BOLD;
        } else if (textWeight.equals("thin")) {
            //sFile = "Lato-Thin.ttf";
            typeface = LATO_THIN;
        } else if (textWeight.equals("hairline")) {
            //sFile = "Lato-Hairline.ttf";
            typeface = LATO_HAIRLINE;
        } else if (textWeight.equals("medium")) {
            //sFile = "Lato-Medium.ttf";
            typeface = LATO_MEDIUM;
        } else if (textWeight.equals("italic")) {
            typeface = LATO_ITALIC;
        } else {
            typeface = LATO_REGULAR;
        }

        setTypeface(typeface);


        //Log.v("CustomTextView " , "typeface = " + sFile);
    }
    
    /*
     * On changing background resource of TextView to a drawable/9-patch image, the pre-defined padding vanishes
     * This is a bug in android
     * To handle this issue, we are overriding the setBackground method for customTextView to retain the padding
     */

    public void setBackground(Drawable background) {
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();

        super.setBackground(background);

        this.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
    }

    public void setTextAppearance(int resId) {
        if (Build.VERSION.SDK_INT < 23) {
            super.setTextAppearance(mContext, resId);
        } else {
            super.setTextAppearance(resId);
        }

        final TypedArray a = mContext.obtainStyledAttributes(resId, R.styleable.NavigationItem);

        if (a.hasValue(R.styleable.NavigationItem_textWeight)) {
            try {
                String textWeight = a.getString(R.styleable.NavigationItem_textWeight).toLowerCase();
                setCustomTypeface(textWeight);
            } catch (Exception e) {
            }
        } else {
            setCustomTypeface("regular");
        }

        a.recycle();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mScale && isEnabled())
        {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Animation animation = new ScaleAnimation(1f, 0.9f, 1f, 0.9f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                animation.setDuration(100);
                animation.setFillAfter(true);
                startAnimation(animation);
            } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
                Animation animation = new ScaleAnimation(0.9f, 1f, 0.9f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                animation.setDuration(100);
                animation.setFillAfter(true);
                startAnimation(animation);
            }
        }
        return super.onTouchEvent(event);
    }

}
//Typeface typeface = Typeface.createFromAsset(context.getAssets(),"Avenir.ttc");
