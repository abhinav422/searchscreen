package com.example.searchscreen.patternlib;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.searchscreen.R;

///
// This text block is used to show "continue reading" image
///
public class CareTextBlock extends FrameLayout
{
    ///
    // Constants
    ///
    private static final int MAX_LINES = 1000;

    ///
    // View members
    ///

    ///
    // Data members
    ///
    private Context     mContext;
    private boolean     mContinueReadingMode;
    private boolean     mIsEditing;
    private int         mMaxLines;
    private String      mValue;
    //default header_4(bold), if need normal use this variable
    private boolean      mIsTextWeightNormal;

    ///
    // Text watcher
    ///
    private TextWatcher mTextWatcher = new TextWatcher()
    {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3)
        {
            // do nothing
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3)
        {
            // do nothing
        }

        @Override
        public void afterTextChanged(Editable editable)
        {
            mValue              = editable.toString();
            TextView textView   = (TextView) findViewById(R.id.textBlockValue);

            textView.setText(mValue);
        }
    };

    ///
    // Constructor
    ///
    public CareTextBlock(Context context)
    {
        this(context, null, 0);
    }

    ///
    // Constructor
    ///
    public CareTextBlock(Context context, AttributeSet attrs)
    {
        this(context, attrs, 0);
    }

    ///
    // Constructor
    ///
    public CareTextBlock(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);

        mContext                = context;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view               = inflater.inflate(R.layout.text_block, this, true);

        if (attrs != null)
        {
            TypedArray a           = context.obtainStyledAttributes(attrs, R.styleable.CareTextBlock, 0, 0);
            mMaxLines              = a.getInt(R.styleable.CareTextBlock_maxLines, MAX_LINES);
            mContinueReadingMode   = a.getBoolean(R.styleable.CareTextBlock_continueReading, false);
            mIsEditing             = a.getBoolean(R.styleable.CareTextBlock_isEditing, false);
            mIsTextWeightNormal    = a.getBoolean(R.styleable.CareTextBlock_textBlockValue_textWeight_normal, false);

            TextView valueView     = (TextView) findViewById(R.id.textBlockValue);
            // default header_4(bold), if need normal use this variable
            int textStyle;

            if (mIsTextWeightNormal) {
                textStyle = R.style.header_4_normal;
            } else {
                textStyle = R.style.header_4;
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                valueView.setTextAppearance(textStyle);
            } else {
                valueView.setTextAppearance(context, textStyle);
            }

            // Find value view's params
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) valueView.getLayoutParams();

            int valueMarginTop     = a.getDimensionPixelSize(R.styleable.CareTextBlock_textBlockValue_marginTop, params.topMargin);
            int valueMarginBottom  = a.getDimensionPixelSize(R.styleable.CareTextBlock_textBlockValue_marginBottom, params.bottomMargin);
            int valueMarginLeft    = a.getDimensionPixelSize(R.styleable.CareTextBlock_textBlockValue_marginLeft, params.leftMargin);
            int valueMarginRight   = a.getDimensionPixelSize(R.styleable.CareTextBlock_textBlockValue_marginRight, params.rightMargin);

            if ((valueMarginTop != 0) || (valueMarginBottom != 0) || (valueMarginLeft != 0) || (valueMarginRight != 0))
            {
                params.setMargins(valueMarginLeft, valueMarginTop, valueMarginRight, valueMarginBottom);
            }

            EditText editText = (EditText) findViewById(R.id.textBlockValueEdit);

            // Find edit view's params
            params = (LinearLayout.LayoutParams) editText.getLayoutParams();

            int editMarginTop     = a.getDimensionPixelSize(R.styleable.CareTextBlock_textBlockValueEdit_marginTop, params.topMargin);
            int editMarginBottom  = a.getDimensionPixelSize(R.styleable.CareTextBlock_textBlockValueEdit_marginBottom, params.bottomMargin);
            int editMarginLeft    = a.getDimensionPixelSize(R.styleable.CareTextBlock_textBlockValueEdit_marginLeft, params.leftMargin);
            int editMarginRight   = a.getDimensionPixelSize(R.styleable.CareTextBlock_textBlockValueEdit_marginRight, params.rightMargin);

            if ((editMarginTop != 0) || (editMarginBottom != 0) || (editMarginLeft != 0) || (editMarginRight != 0))
            {
                params.setMargins(editMarginLeft, editMarginTop, editMarginRight, editMarginBottom);
            }
        }
        applyValues();
    }

    ///
    // Get current number of characters typed
    ///
    public int getCurrentLength()
    {
        EditText editText = (EditText) findViewById(R.id.textBlockValueEdit);
        return editText.length();
    }

    ///
    // Returns the current value
    ///
    public String getValue()
    {
        return mValue;
    }

    ///
    // Sets selection
    ///
    public void setSelection(int selection)
    {
        final EditText editText = (EditText) findViewById(R.id.textBlockValueEdit);
        final int _selection    = selection;

        editText.post(new Runnable()
        {
            @Override
            public void run()
            {
                editText.setSelection(_selection);
            }
        });
    }

    ///
    // Sets value
    ///
    public void setValue(String value)
    {
        mValue = value;
        applyValues();
    }

    ///
    // Sets edit/view mode
    ///
    public void setEditing(boolean editing)
    {
        mIsEditing = editing;
        applyValues();
    }

    ///
    // Refreshes the views
    ///
    public void refresh()
    {
        invalidate();
        requestLayout();
    }

    ///
    // Applies values to subviews
    ///
    private void applyValues()
    {
        // Set the value.
        TextView text = (TextView) findViewById(R.id.textBlockValue);
        text.setText(mValue);

        EditText editText = (EditText) findViewById(R.id.textBlockValueEdit);
        editText.setText(mValue);

        View continueReadingContainer = findViewById(R.id.continueReadingContainer);

        // Edit mode
        if (mIsEditing)
        {
            editText.setVisibility(View.VISIBLE);
            text.setVisibility(View.GONE);
            continueReadingContainer.setVisibility(View.GONE);
            editText.addTextChangedListener(mTextWatcher);
        }
        // View mode
        else
        {
            text.setVisibility(View.VISIBLE);
            editText.setVisibility(View.GONE);

            if (mContinueReadingMode)
            {
                setUpContinueReadingMode(text);
            }
            else
            {
                continueReadingContainer.setVisibility(GONE);
            }
            editText.removeTextChangedListener(mTextWatcher);
        }
    }

    ///
    // Setting up Continue Reading mode.
    ///
    private void setUpContinueReadingMode(final TextView text)
    {
        final View continueReadingContainer = findViewById(R.id.continueReadingContainer);

        // Doing after a delay so that we get the line count.
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                if (text.getLineCount() > mMaxLines)
                {
                    text.setMaxLines(mMaxLines);
                    continueReadingContainer.setVisibility(VISIBLE);
                    findViewById(R.id.shadow_bg).setVisibility(VISIBLE);
                }
                else
                {
                    findViewById(R.id.shadow_bg).setVisibility(GONE);
                    continueReadingContainer.setVisibility(GONE);
                }
            }
        }, 100);

        // Setting a click listener.
        continueReadingContainer.setOnClickListener( new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                continueReadingContainer.setVisibility(GONE);
                findViewById(R.id.shadow_bg).setVisibility(GONE);
                ((TextView) findViewById(R.id.textBlockValue)).setMaxLines(MAX_LINES);
            }
        });
    }
}

