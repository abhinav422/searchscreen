package com.example.searchscreen

import android.content.Context
import dagger.Component
import okhttp3.OkHttpClient
import javax.inject.Named
import javax.inject.Singleton

@Component(modules = arrayOf(AppComponentModule::class))
@Singleton
interface AppComponent{

     fun inject(searchScreenApplication: SearchScreenApplication)
    fun getOkHttp():OkHttpClient
    fun getOkHttpClientAndProvider():OkHttpAndServerProvider
    @Component.Builder
    interface Builder{
        fun build():AppComponent
        fun appComponentModule(appComponentModule: AppComponentModule):Builder
    }
}