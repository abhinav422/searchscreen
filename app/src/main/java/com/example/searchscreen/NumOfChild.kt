package com.example.searchscreen

enum class NumOfChild(key:String,value: String){
    ANY("Any",""),ONE("1","1"),TWO_OR_LESS("2 or less","2"),THREE_OR_LESS("3 or less","3");
    public val value:String=value
    public val key:String=key
    companion object{
        fun getNumOfChildValue(text:String):String{
            val numOfChild= NumOfChild.values()
            for(i in 0..numOfChild.size-1){
                if(text.equals(numOfChild[i].key)){
                    return numOfChild[i].value
                }
            }
            return ANY.value
        }
        fun getNumOfChildKey(text:String):String{
            val numOfChild= NumOfChild.values()
            for(i in 0..numOfChild.size-1){
                if(text.equals(numOfChild[i].value)){
                    return numOfChild[i].key
                }
            }
            return ANY.key
        }
    }


}