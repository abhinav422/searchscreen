package com.example.searchscreen

import dagger.Module
import dagger.Provides

@Module
class JobDetailsComponentModule{

    @Provides
    @JobDetailsActivityScope
    fun getSearchType():SearchType{
        return SearchType.JOB_DETAIL
    }
}