package com.example.searchscreen

import java.io.Serializable

data class SearchParams(var mServiceId: String, var mZipCode: String,
                        var mDist: String,
                        var mStart: String,
                        var mMax: String,
                        var mSortBy: String,
                        var apiKey: String,
                        var mVersion: String,
                        var mBuildNo: String,
                        var mOS: String,
                        var mOSVersion: String,
                        var mAuthToken: String,
                        var mSearchFilter:SearchFilter):Serializable {


    fun getPath(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
