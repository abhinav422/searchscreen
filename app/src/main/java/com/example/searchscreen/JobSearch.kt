package com.example.searchscreen

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import javax.inject.Inject

class JobSearch @Inject constructor(var apiRequest: ApiRequest){


    companion object {
        public var mServiceId: String = "CHILDCARE"
         var mKeyword: String = ""
         var mZipCode: String = "02452"
        var mDist: String = "10"
        var mHourlyRate: String = ""
        var mJobType: String=""
        var mNumOfChild: String = ""
        var mAgeOfChild: String = ""
        var mJobCategory: String = ""
    }
    var mStart: Int = 0
    var jobsList:Hashtable<Int,JobsList> = Hashtable()
    var mutableLiveData:MutableLiveData<Hashtable<Int,JobsList>> = MutableLiveData()
    var sortBy:String = "BEST"
    init {
        apiRequest.mutableLiveData.observeForever(object :Observer<JSONObject>{
            override fun onChanged(jsonObject: JSONObject?) {
                var count = (jsonObject?.get("data") as JSONObject).get("jobsCount") as Int
                var jobsList:JSONArray =(jsonObject?.get("data") as JSONObject).getJSONArray("JobsList")
                for(i  in 0..jobsList.length()-1){
                   var jobsJsonObject : JSONObject =  jobsList.getJSONObject(i) as JSONObject
                    var job:JobsList = Gson().fromJson(jobsJsonObject.toString(),JobsList::class.java)
                    this@JobSearch.jobsList.put(i + searchParams.mStart.toInt(),job)

               }
                // Check .value assignment
                mutableLiveData.postValue(this@JobSearch.jobsList)
            }
        })
    }

     var searchParams = SearchParams(mServiceId,"02452","10","0","25",sortBy,"TfbS5OSlsvW3XoaSFrI3CZ5JKSJQDSJnBTOZbviwdr0x"
            ,"9.5.1","1066","android","21","O9w7Nyp47WGaDWHVX84P1nY7mxbN89DewQ_6GdKEYEI.",SearchFilter("","","","",
            false,false,"","",false,false))



    public fun getListOfParams(searchParams: SearchParams):ArrayList<Params>{
        var urlParams:ArrayList<Params> = ArrayList()
        urlParams.add(Params("serviceId", searchParams.mServiceId))
        urlParams.add(Params("zipCode", searchParams.mZipCode))
        urlParams.add(Params("radius", searchParams.mDist))
        urlParams.add(Params("start", searchParams.mStart))
        urlParams.add(Params("max", searchParams.mMax))
        urlParams.add(Params("sortByColumn", searchParams.mSortBy))

        if (searchParams.mSearchFilter != null) {
            val filter = searchParams.mSearchFilter
            urlParams.add(Params("keyWord", filter.mKeyword))
            urlParams.add(Params("hourlyRate", filter.mHourly))
            urlParams.add(Params("ccNumberOfChildren", filter.mNumOfChildren))
            urlParams.add(Params("ccChildrenAges", filter.mChildrenAges))
            urlParams.add(Params("noTransportationNeeded", getBooleanAsString(filter.mTransp)))
            urlParams.add(Params("hasPhoto",getBooleanAsString( filter.mPhoto)))
            urlParams.add(Params("recentlyActive", getBooleanAsString(filter.mRecent)))
            urlParams.add(Params("jobType", filter.mJobType))
        }
        return urlParams
    }

    fun getBooleanAsString(bool:Boolean):String{
        if(bool){
            return "true"
        }
        return "false"
    }


    fun startRequest(){
        searchParams = SearchParams(mServiceId, mZipCode, mDist,mStart.toString(),"25",sortBy,"TfbS5OSlsvW3XoaSFrI3CZ5JKSJQDSJnBTOZbviwdr0x"
                ,"9.5.1","1066","android","21","O9w7Nyp47WGaDWHVX84P1nY7mxbN89DewQ_6GdKEYEI.",SearchFilter(mKeyword, mHourlyRate, mNumOfChild, mAgeOfChild,
                false,false, mJobType, mJobCategory,false,false))
        apiRequest.startRequest(getListOfParams(searchParams))
    }

    fun clearJobSearch() {
        jobsList=Hashtable()
    }
}