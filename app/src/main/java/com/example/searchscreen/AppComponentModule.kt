package com.example.searchscreen

import android.content.Context
import dagger.Module
import dagger.Provides
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import javax.inject.Named
import javax.inject.Singleton

@Module
class AppComponentModule constructor(private var applicationContext: Context){




    @Provides
    @Singleton
    public fun getOkHttpClient() : OkHttpClient{
       return  OkHttpClient.Builder().addInterceptor(object : Interceptor {
            override fun intercept(chain: Interceptor.Chain?): Response? {
                val request = chain?.request()
                val authenticatedRequest = request?.newBuilder()
                        ?.header("Authorization", Credentials.basic("remote","remoteUserP!"))?.build()

                return chain?.proceed(authenticatedRequest)

            }

        }).build()
    }

    @Provides
    fun getOkHttpClientAndServer() : OkHttpAndServerProvider{
        return OkHttpAndServerProvider()
    }


}