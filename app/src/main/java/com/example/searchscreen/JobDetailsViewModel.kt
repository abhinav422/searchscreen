package com.example.searchscreen

import android.arch.core.util.Function
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import javax.inject.Inject

class JobDetailsViewModel:ViewModel(){

    fun startRequest(jobId: Long) {
        jobDetailsSearch.startRequest(jobId)
    }

    @Inject
    lateinit var jobDetailsSearch: JobDetailsSearch

    lateinit var jobDetailsLiveData: LiveData<JobDetails>
    init{
        DaggerJobDetailsComponent.builder().appComponent(SearchScreenApplication.getInstance().appComponent).build().inject(this)
       jobDetailsLiveData= Transformations.switchMap(jobDetailsSearch.jobsDetailLiveData,object : Function<JobDetails, LiveData<JobDetails>>{
            override fun apply(input: JobDetails?): MutableLiveData<JobDetails> {
                var jobDetailsLiveData:MutableLiveData<JobDetails> = MutableLiveData()
                jobDetailsLiveData.postValue(input)
                return jobDetailsLiveData
            }

        })
    }


}