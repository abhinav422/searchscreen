package com.example.searchscreen

import java.io.Serializable

data class SearchFilter(var mKeyword:String,var mHourly : String , var mNumOfChildren : String,
                        var mChildrenAges: String,var mTransp: Boolean,var mPhoto:Boolean,var mJobType: String,
                        var mJobCategory : String, var mRecent : Boolean,var mHidePassedJobs : Boolean) : Serializable
